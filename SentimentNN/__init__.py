import logging
import tensorflow as tf
import string
import re
import json
import numpy as np
from tensorflow import keras

import azure.functions as func

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    def custom_standardization(input_data):
        lowercase = tf.strings.lower(input_data)
        stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
        return tf.strings.regex_replace(stripped_html,
                                        '[%s]' % re.escape(string.punctuation),
                                        '')
                                        
    req_body = req.get_json()
    tweets = req_body.get('tweet')
    if tweets:
        model = keras.models.load_model('./model' , custom_objects={'custom_standardization': custom_standardization})
        response = model.predict([tweets])
        if response[0][0] > 0.6:
            sentiment = 'positive'
        elif response[0][0] < 0.4:
            sentiment = 'negative'
        else:
            sentiment = 'neutral'
        return func.HttpResponse(
            str("the tweet is {} with a score of {}".format(sentiment, response[0][0])),
            status_code=200
        )
    else:
        return func.HttpResponse(
            "Tweets not supplied in correct format",
            status_code=400
        )