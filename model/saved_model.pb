��
�&�&
D
AddV2
x"T
y"T
z"T"
Ttype:
2	��
^
AssignVariableOp
resource
value"dtype"
dtypetype"
validate_shapebool( �
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
K
Bincount
arr
size
weights"T	
bins"T"
Ttype:
2	
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
�
Cumsum
x"T
axis"Tidx
out"T"
	exclusivebool( "
reversebool( " 
Ttype:
2	"
Tidxtype0:
2	
R
Equal
x"T
y"T
z
"	
Ttype"$
incompatible_shape_errorbool(�
=
Greater
x"T
y"T
z
"
Ttype:
2	
�
HashTableV2
table_handle"
	containerstring "
shared_namestring "!
use_node_name_sharingbool( "
	key_dtypetype"
value_dtypetype�
.
Identity

input"T
output"T"	
Ttype
l
LookupTableExportV2
table_handle
keys"Tkeys
values"Tvalues"
Tkeystype"
Tvaluestype�
w
LookupTableFindV2
table_handle
keys"Tin
default_value"Tout
values"Tout"
Tintype"
Touttype�
b
LookupTableImportV2
table_handle
keys"Tin
values"Tout"
Tintype"
Touttype�
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
�
Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
>
Maximum
x"T
y"T
z"T"
Ttype:
2	
�
Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�
>
Minimum
x"T
y"T
z"T"
Ttype:
2	
?
Mul
x"T
y"T
z"T"
Ttype:
2	�
�
MutableHashTableV2
table_handle"
	containerstring "
shared_namestring "!
use_node_name_sharingbool( "
	key_dtypetype"
value_dtypetype�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
�
PartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
�
Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
�
RaggedTensorToTensor
shape"Tshape
values"T
default_value"T:
row_partition_tensors"Tindex*num_row_partition_tensors
result"T"	
Ttype"
Tindextype:
2	"
Tshapetype:
2	"$
num_row_partition_tensorsint(0"#
row_partition_typeslist(string)
@
ReadVariableOp
resource
value"dtype"
dtypetype�
�
ResourceGather
resource
indices"Tindices
output"dtype"

batch_dimsint "
validate_indicesbool("
dtypetype"
Tindicestype:
2	�
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
A
SelectV2
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
m
StaticRegexReplace	
input

output"
patternstring"
rewritestring"
replace_globalbool(
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
<
StringLower	
input

output"
encodingstring 
e
StringSplitV2	
input
sep
indices	

values	
shape	"
maxsplitint���������
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.9.12v2.9.0-18-gd8ce9f9c3018�
z
Adam/dense/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameAdam/dense/bias/v
s
%Adam/dense/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense/bias/v*
_output_shapes
:*
dtype0
�
Adam/dense/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/dense/kernel/v
{
'Adam/dense/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/v*
_output_shapes

:*
dtype0
�
Adam/embedding/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�N*,
shared_nameAdam/embedding/embeddings/v
�
/Adam/embedding/embeddings/v/Read/ReadVariableOpReadVariableOpAdam/embedding/embeddings/v*
_output_shapes
:	�N*
dtype0
z
Adam/dense/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameAdam/dense/bias/m
s
%Adam/dense/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense/bias/m*
_output_shapes
:*
dtype0
�
Adam/dense/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/dense/kernel/m
{
'Adam/dense/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/m*
_output_shapes

:*
dtype0
�
Adam/embedding/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�N*,
shared_nameAdam/embedding/embeddings/m
�
/Adam/embedding/embeddings/m/Read/ReadVariableOpReadVariableOpAdam/embedding/embeddings/m*
_output_shapes
:	�N*
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_2
[
count_2/Read/ReadVariableOpReadVariableOpcount_2*
_output_shapes
: *
dtype0
b
total_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_2
[
total_2/Read/ReadVariableOpReadVariableOptotal_2*
_output_shapes
: *
dtype0
b
count_3VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_3
[
count_3/Read/ReadVariableOpReadVariableOpcount_3*
_output_shapes
: *
dtype0
b
total_3VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_3
[
total_3/Read/ReadVariableOpReadVariableOptotal_3*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
}
MutableHashTableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name
table_94*
value_dtype0	
l

hash_tableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name1442*
value_dtype0	
l

dense/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_name
dense/bias
e
dense/bias/Read/ReadVariableOpReadVariableOp
dense/bias*
_output_shapes
:*
dtype0
t
dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_namedense/kernel
m
 dense/kernel/Read/ReadVariableOpReadVariableOpdense/kernel*
_output_shapes

:*
dtype0
�
embedding/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�N*%
shared_nameembedding/embeddings
~
(embedding/embeddings/Read/ReadVariableOpReadVariableOpembedding/embeddings*
_output_shapes
:	�N*
dtype0
G
ConstConst*
_output_shapes
: *
dtype0	*
value	B	 R
H
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 
I
Const_2Const*
_output_shapes
: *
dtype0	*
value	B	 R 
I
Const_3Const*
_output_shapes
: *
dtype0	*
value	B	 R 
�
Const_4Const*
_output_shapes	
:�N*
dtype0*��
value��B���NBtheBandBaBofBtoBisBinBitBiBthisBthatBwasBasBforBwithBmovieBbutBfilmBonBnotByouBareBhisBhaveBbeBheBoneBitsBallBatBbyBanBtheyBfromBwhoBsoBlikeBherBjustBorBaboutBhasBoutBifBsomeBthereBwhatBgoodBmoreBwhenBveryBevenBmyBsheBupBnoBtimeBwouldBwhichBonlyBreallyBstoryBtheirBwereBhadBseeBcanBmeBthanBweBmuchBwellBbeenBgetBwillBalsoBotherBpeopleBbadBintoBdoBfirstBbecauseBgreatBhimBhowBmostBdontBmadeBmoviesBthenBthemBfilmsBwayBmakeBanyBcouldBtooB
charactersBafterBthinkBwatchBtwoBseenB	characterBmanyBbeingBactingBneverBplotBlittleBbestBwhereBloveBlifeBdidBknowBshowBdoesBeverBbetterByourBendBstillBoverBoffBhereBtheseBmanBsayBwhileBwhyBsceneBsuchBscenesBgoB	somethingBshouldBthroughBimBbackBthoseBdoesntBrealBwatchingBthoughBnowByearsBthingBactorsBdidntBbeforeBanotherBnothingBnewBactuallyBmakesBworkBfunnyBoldBlookBfindBsameBeveryBfewBusBgoingBagainBpartBlotBdirectorBcastBcantBthingsBquiteBthatsBwantBprettyBseemsByoungBworldBaroundBgotBdownBfactBenoughBbetweenBhoweverBtakeBbothBhorrorBgiveBmayBiveBownBthoughtBoriginalBbigBseriesBgetsBwithoutBcomeBalwaysBrightBtimesBisntBsawBlongBwholeBleastBpointBtheresBroleBfamilyBactionBalmostBinterestingBmustBcomedyBbitBmusicBdoneBscriptBguyBminutesBmightBanythingBlastBsinceBfarBfeelBhesBperformanceBprobablyBkindBamBawayBratherByetBworstBsureBtvBfunBeachBwomanBgirlBplayedBmakingBanyoneBfoundBourBhavingBalthoughBcomesBbelieveBdayBtryingBcourseB
especiallyBgoesBlooksB	differentBplaceBhardBshowsBbookBputBwasntBendingBsenseBonceBreasonBtrueBmainB
everythingBscreenBmaybeBmoneyBworthBtogetherBsomeoneBjobBactorB2BsetBdvdBlookingBwatchedBplaysBinsteadBsaidBseemBtakesB10BplayBthreeBlaterB	beautifulBeffectsBhimselfBeveryoneBduringB	excellentBaudienceBjohnBleftBspecialBhouseBversionBseeingBnightBamericanBideaBshotBniceBwifeBsimplyBhelpBelseBblackBlessBreadBdeathBstarBfanByoureBkidsBwarBusedBhighBfatherBsecondB
completelyBfriendsBwrongByearBeitherBmindBgivenBperformancesBpoorBuseBboringBtryBhomeBenjoyBmenBneedBshortBrestBclassicBtrulyB	hollywoodBuntilBlineBnextBalongB
productionBtellBcoupleBdeadBhalfBothersBawfulBrememberB	recommendBstartBmomentsBletBcameBkeepBepisodeBperhapsBfullB
understandBstupidBcameraBstarsBmeanBwomenBterribleBdoingBgettingBplayingB	wonderfulB
definitelyBsexBsmallBhumanBgivesBoftenBnameBperfectBearlyBbecomeBvideoBdialogueBfaceBcaseBitselfBcouldntBlinesBfinallyBpersonBfeltB
absolutelyBwasteBlostBtitleBsupposedBschoolBpieceBlikedBbudgetBtopBentireBwrittenBliveByesBworseBhopeBagainstBentertainingBwentBsortBheadBproblemBshesBpictureBoverallB	certainlyBstyleBcinemaB	beginningBevilBboyBcareBseveralBfansBwhiteBlivesBbasedBmotherBlovedBexampleB	directionBdarkBohBalreadyBmrBseemedBidB3BwantedBbecomesBchildrenB
throughoutBunfortunatelyBturnBguysBdespiteBamazingBwontBfinalBkillerBfriendBhistoryBfineBguessBtotallyBlaughB1BhumorBwantsBdaysBsoundBgirlsBdramaBbehindBlowBworksByoullBmichaelBleadBBturnsBcalledBqualityBpastBsonBgameBableBtriesBgaveBwritingBactBunderBfavoriteBtheyreBstartsBenjoyedBhorribleBgenreBsideB	brilliantBexpectBtownBviewerBeyesBflickBkillB
themselvesBstoriesBonesB	sometimesBactressBthinkingBsoonBcarBpartsBdirectedB	obviouslyBstuffBheartBartBfeelingBbloodBdecentBrunBmatterBetcBfightBsaysBmyselfBleaveBchildBlateBillBhighlyBcannotBhandBcityBcloseBkilledBheardBexceptBmomentBrolesBhellBtookBwouldntBhappenedBstrongBkidBcompleteBlackBparticularlyBpoliceBtoldBhappensBhappenBhourBanywayBinvolvedBwonderBdaughterBattemptBviolenceB	extremelyBjamesBvoiceBchanceBobviousBlivingBcomingBnoneBmurderBaloneBshownBpleaseB	includingBscoreB
experienceBsaveBsimpleBlookedBageBgroupBcrapBagoBannoyingBtypeBgoreBgodBcinematographyBtakenBsongBseriousBinterestBslowBsadBbrotherBexactlyBstartedBokBhoursBpossibleBendsByourselfBusuallyBletsBcareerBreleasedBusualB	hilariousBjokesBstopBdavidBscaryBhitBacrossBmusicalBwhoseBnumberBcoolBhugeB	seriouslyBfindsBrelationshipBknownBchangeBrunningBopinionBopeningBmostlyBfemaleBtodayBepisodesBtalentBrobertBcutBbodyB
ridiculousBnovelBsayingBshotsBenglishBsomewhatBeventsBstrangeBpowerBmajorBhappyBrealityBorderB	basicallyBtalkingB	importantBheroB5BlevelBwishBknewBtakingBdisappointedBeasilyB4BroomBviewB	attentionBtellsBhusbandB	directorsBcallBwordsB
apparentlyBsingleB
supportingBearthBkingBbritishBproblemsBknowsBdueBdocumentaryBsillyBsongsBclearlyBarentBlocalBwhatsBratingBlightBfourBturnedBsimilarBcheapBwordBpaulBfallsBfutureBjackBsetsBbeyondBwhetherB
televisionBcomicBmodernBviewersBcountryBreviewB	animationBmissBfiveBsequenceBbringBuponBrichardBrockBgeorgeBpredictableBwithinBgivingBappearsBsequelB	enjoyableBtalkBladyBaddBthemeBactualBherselfBbunchBromanticB	storylineBpointsBfeelsBtheaterBredBlotsBmessageBneedsBmysteryBentertainmentBaboveB	surprisedBnearlyBhaventBamongBtenBteamBparentsBmovingBmentionBdullBtomBwaysBreleaseBfallBbeginsBseasonBeasyBnamedBeffortBdialogB	fantasticBelementsBcommentsBtriedBworkingBtypicalByorkBgeneralBpeterBavoidBthrillerBsomehowBmiddleBcertainBnearBfeatureBusingBstraightBeyeBleadsBformBclassBkeptBfigureBwriterBcheckBweakBtaleBfamousBbuyBhateBgreatestB	sequencesBdoubtBshowingB
particularBmaterialBfrenchBsorryBlameBeditingBspaceBclearBmeansBgoneBstayBsisterB	realisticBfilmedB
soundtrackBleeBwaitBlearnBdecidedBfollowBviewingBimagineBfastBdieBpoorlyBoscarBdealBhearBreviewsBdanceB
atmosphereBtruthB
eventuallyBmoveByouveBcrimeBthirdBsurpriseBperiodBwhosBindeedBbroughtBsuspenseBsexualBkillingBwritersBwhateverBsubjectBnatureBforgetBzombieBsitBexpectedB
believableB80sBpremiseBdeBpossiblyB
screenplayBrentBnorB
filmmakersBjapaneseB	emotionalBromanceBbabyBbeginBnoteBaverageBstandBneededBokayBleavesB	difficultB	memorableBmeetsB	situationB	otherwiseBquestionBdramaticBdisneyBboysBsuperbBdrBbecameBcreditsBfootageBdogBshameBreadingBforcedBrealizeBearlierBwriteBstageBolderBmeetBstreetBsoundsBcommentBbadlyBaskBquicklyBbringsBweirdBtotalBminuteBamericaBwhomBunlessBplentyBkeepsBfeaturesBfreeBsocietyBdevelopmentB
interestedBcreateBmessBimdbBbeautyBsettingBdeepBworkedBeffectBcastingBresultB
incrediblyB	potentialBhotBcrazyB	directingBtowardsBplusBpersonalBcreepyBbBmaleBmeantBmarkBvariousB	perfectlyBdreamBlaughsBreturnBhandsBbattleBapartB20BadmitBhardlyBuniqueBfailsBbusinessBcheesyBremakeBpreviousBopenBmissingBleadingBairBmasterpieceBmanagesBinsideBdumbBforwardBsecretBfantasyBjoeBscifiBreasonsB	portrayedBjokeBpowerfulBappearBideasBfireB70sBbrothersBmatchB	christmasB
backgroundBsuccessBrecentlyBmonsterBbillBunlikeBtwistBfightingBattemptsBdeservesBoutsideBfairlyBspoilersBfrontB	followingBbreakB	politicalBwilliamBlaBtellingBtalentedBcopyBpureBpresentBagreeBpayBflatBcuteBmarriedBjaneBvillainBmissedBwesternBwastedBcaughtBnudityB	expectingBeraBcopBbenBspentBrichBneitherB
incredibleBgayBactedBplainBsadlyBdoctorBholdBcreatedB
girlfriendBfurtherBcomparedBmembersBcrewBboxBbooksBanimatedBpublicBfearBzombiesBseesBescapeBendedBcauseBslightlyBdecidesBsweetBpaceBislandBpartyBsuddenlyBrateB	mentionedBlistBlargeBtensionBusesBoddBconsideringBconceptBwaitingBfamiliarBentirelyB	effectiveBintelligentBdiedB	audiencesBlaughingBcoverBchoiceBwroteBsocialBmovesBcreditBcleverBbasicBtroubleB	portrayalBlanguageBboredBvisualB
convincingBcartoonBmadBrevengeBbandBscottBpopularBrecentBpositiveBexcitingBcommonBwaterBviolentBspiritBfilledBdepthBvanB
successfulBofficeB12B
ultimatelyB	pointlessBconsiderBdancingB
appreciateBadultBspendBmaryBkillsBgunBcoldBbizarreBbiggestBvalueB	somewhereBscienceBproducedBcatBspeakB	producersBformerBcompanyB	chemistryBstateB7ByoungerB8BsolidB
situationsBsickBfollowsBamountBprojectBgermanBcontrolBcenturyBamusingBrespectB	questionsBfocusB
conclusionBwonBwalkBslasherBfitB
impressiveBwinBwerentBsingingBshowedBleavingBfailedBtripBrunsBrecommendedBplanetB
disturbingBtonyBstoreBawesomeBtouchBtoneBdecideB
impossibleBimmediatelyBhairBchangedBbarelyBstudioB	involvingBsouthB
adaptationBimagesBpatheticBitalianBhonestlyBcampBasideBaccentBthanksBprisonBmagicB15BlongerB	literallyBlikesBcharmingBgladBforceBfakeBjimBcollegeBvaluesBsurprisinglyBstandardBghostBfrankB	generallyBcultBaspectBgarbageBstarringBknowingBgeniusBnormalBmeaningBshootingBpicturesBtrashBsmithB	detectiveBsteveBsittingBcomediesBabilityB30BstickBcatchBaliveB
personallyBboughtBwestBnaturalBhonestB
appearanceBsexyBfairBbeautifullyBtoughBnowhereBarmyBcomputerBattackBappealBvampireBsoldiersBhumourB
consideredB	adventureBwoodsBthinksBtasteBsamBpurposeBexplainBtwistsBmanagedBdadBremainsByeahBlondonBtermsBsubtleBplanBfullyBchrisBmasterBchannelBpassBmistakeBdisappointingButterlyBtouchingBexcuseBpickBmilitaryBdreamsBcultureBoutstandingBwalkingBterrificBmoodBlovesBthankBrareBmannerBchaseB100BnobodyBunbelievableBsilentBlovelyBjourneyBcharmBnakedBlikelyB	laughableBequallyBdateBcostumesBdisappointmentBbatmanBselfBpresenceBthemesBstunningBspoilerBcryBmakeupBinnocentBcontainsBcomplexBanimalsByoudBthusBloudBclimaxBslowlyBtrainBthrownB
impressionBbottomBaddedBweekBjusticeB
governmentBedgeB
constantlyBbossBplacesBhopingBfinishBfictionBdoorBchristopherBvictimsBsurelyBrideBissuesB	exceptionBdetailsBsentBroadBpainfulBemotionBwildB	presentedBdrawnBbrainBmakersBmainlyBharryBcharlesBminorBlawBheyBexpectationsBawardB	narrativeBmarriageBintendedBwowBbruceBstandsBpiecesBphotographyBnamesB	cinematicBchurchBbesidesBaspectsBstewartBsceneryBparisBheavyBsoulBputsBofferBindianBclubBthrowB
mysteriousBfeelingsBcentralBbriefB9BvictimBtrackBsupposeBratedBgangBfallingB
differenceBcriticsBtwiceBfestivalB	developedBcolorBshootBmansBfascinatingBchangesBbotherBsuggestB
historicalBtiredBsmartBserialBlikableB	availableBactsBhasntBbedBopportunityBlaughedBfilmingBemotionsBblueBelementBdiesBbuildingBapproachBaheadBtrailerB
supposedlyBimpactBcreativeB	confusingBsummerBgiantBfollowedBflicksBjerryBincludeB6B	happeningBsystemBfreshB	everybodyBnumbersBadultsBmotionBhotelBgradeBmoralBlivedBkeyBeventBcontentBagentBimaginationBforeverB
compellingBspeakingBnoirBmediocreBlacksB
thoroughlyBsupportBgorgeousBflawsBcharlieB	boyfriendBbornBappearedBmillionBfellB	wonderingBprovidesBhurtBflyingBdriveBzeroB	standardsBnoticeBmurdersBmsBhiddenBfellowBconfusedBpageBjonesBgemBdamnBimageBbarBstudentsBrandomBproducerBpainBlightingBhenryBshouldntB	childhoodBrelationshipsB	impressedBholesBdescribeBalBshareBnegativeBiiBanswerB	seeminglyBputtingBmixBgreenBshockBrentedBlatterBgamesB	christianBlandBdeliversBstuckBraceBprovesBmerelyBbillyBuglyBstepBsecondsBfolksBdrugBdeliverBafraidBtragedyBstudentBoperaBkellyBfunniestBextremeBsevenBloverBhospitalB	americansBoffersBinspiredBhelpsB	filmmakerBdetailBabsoluteB
whatsoeverBsixBpaidBintenseBfacesBbondBaddsBdesignBartisticBaffairBtragicBstatesBrayBindustryBdeeplyBturningBparkBmartinBcompareBadditionBsoldierB	redeemingBheldBcountBcarryBarthurBteenageBskyBpullBpersonalityBdirectB	actressesBstruggleBremindedBquickBqueenBinformationBhumansB	forgottenBpickedBfindingBcriminalBwoodenBjasonBdyingB
collectionBcaptainBasksBalienB90BthinBpornBbecomingBallowedBgroundBfashionBrapeBmovedBgraceBbrianBwarsBuncleBlordB	favouriteB
attractiveBunusualBstoneBprovideBcgiBwonderfullyBnewsBmomBcontinueBaccidentB	thereforeB	technicalBsuperBloseBincludesBfoodBcreatureBareaBangryBsuperiorBpassionBnastyBintelligenceBdrugsBdouglasBdoubleB	desperateBwilliamsBweddingBstephenB	scientistBrealizedBtreatBspotBbeatBanymoreBwearingBremindsB	nightmareB	necessaryBepicBclichéBbeganBworthyBtrustB
remarkableBreadyBplaneBlocationBjoanBholdsBshockingBrussianBontoBfoxBallowBsleepBhelpedBunnecessaryBtearsBnormallyBmentalBlistenBdisasterB	apartmentB	accordingBwallBtimBprofessionalBpleasureBringBledB
introducedBextraBdirtyBdavisBdannyBallenBwarningBtortureBscaredBpowersBgrandBcopsBanywhereBactionsBwillingBteacherBmartialBliesBindependentBgrowingBenergyBcomedicBchineseBapparentBanimalB	watchableBmemberBsmileBrollBmachineBdesireBclothesBbuildBtheatreB
filmmakingBexplanationBcastleBsuspectBshipBsearchBmonstersBhigherBclichésBcaptureBacceptBacademyB60sBtodaysBsatBmemoryBhatedBskipBphysicalBlegendBladiesBabsurdB50BwantingBtowardBmouthBloversBdickBreturnsBrarelyBnicelyBkillersB
commentaryBartsBalanBstationBheroesBengagingBedBconstantBprocessBmediaBjoyB
intriguingBfinestBcreatingBvillainsB
surprisingBsightBjohnnyBgunsBfinishedBfailBbloodyBartistB40BmoonBjapanBbitsBvsBsuitBphoneBpacingBlimitedBdonB	dangerousBblameBandyB	religiousBenglandBsomebodyB
originallyBlackingBjumpBjrB
delightfulBaccurateBwinningBinstanceBieBheavenB	communityBbrutalBplayersBhitsBheresBexistBcarsBvisionBpilotBmonthsBmetBhopesBaskedBanybodyBunknownBkevinB
friendshipBwitchBlovingBkeepingBkindaBjeanBissueBfaithBwheneverBsavingBproveBpeoplesBgeneBeddieBdeserveBconflictBwerewolfBstartingBregularBordinaryBmorganB	explainedBteenBriverBmixedBheadsBeatB1010BsupermanB	screamingBsavedBprinceBportrayBmissionBgottenBworldsBpriceBnickBfightsBcageBcableBvampiresBsucksBrealismBplotsBmemoriesBdeservedBsuicideBprivateB	lowbudgetB	knowledgeB	featuringBfateBfatBdiscoverBcutsBwittyBwhilstB
washingtonBterriblyBtaylorB	reviewersBrecordBquietBlosesBjesusBdrunkBdatedBweveBunderstandingBtreatedBspanishBrubbishBresultsBpulledBmanageBhadntBfredBemptyBcBbiggerBunfunnyBpsychologicalBofficerBnonsenseBheroineB
connectionByouthBtraditionalBlengthBfieldBvhsBseanBpartnerBlooseBhumanityBessentiallyBdrivingB	continuesBbrokenBadamBwindBvisitBstreetsBresponsibleBplayerBjacksonBforcesB
discoveredBbrightBblandBawareBanthonyBsoapBskillsBoppositeBmagnificentBiceBgaryBeffortsBpopBfranklyBeuropeanBcapturedBvisuallyBsegmentBradioBpretentiousBopensBnumerousBlossBkateBgenuineBdealsB	daughtersBcuriousBblondeBbelowBvillageB	naturallyBmurderedBmorningB	concernedBsuddenBspectacularBsingBsignBreceivedBjudgeBfloorBbobBallowsBtalentsBstockBprovedBownerBharrisBgoldBcurrentBblindBwindowBsurviveBrevealedBnoticedBmrsBmikeB	locationsBkongBinternationalBcornyBbreaksBballBversionsBperspectiveBmineB
unexpectedBultimateBrobBlessonBcallsBadviceB1950sBwiseBvisualsBsuckedBseasonsBprogramBluckBhumorousBdevelopBcreatesBbrilliantlyB90sBtheyveBstudyBstandingBscreamBsatireBjimmyBreactionBlogicBincludedBfreedomBdealingBawardsBwellesBsiteBsantaBpairBnationalBjeffBcontextBannaButterBstealB	presidentBpleasantBoccasionallyBhorseBfordBcausedBawkwardBalbertBshopBlearnedBleaderBfinaleBfamiliesBdressedBdebutBcameoBbehaviorBreachBlaughterBgagsBfathersB	existenceB	discoversBboardBreligionBgrantBformulaBcenterBcandyBakaBtypesB
technologyBstereotypesBspeedBshallowBseaBmilesBgrewB	genuinelyBfavorBdanBbuddyBthomasB
rememberedB
referencesBparkerBoverlyB	deliveredBdecadeBcostsB
comparisonBalexBagesBwoodB
underratedBunableBsheerBsakeBmajorityBgonnaBcrappyBbetBunrealisticBstevenBneverthelessBgraphicBbuiltBaskingBwitBspoilBrulesBkeatonBgangsterBfaultBeveningBericBentertainedBembarrassingBdBbankB
theatricalBtarzanBstrengthBriseBpatrickB	meanwhileBeditedBdecisionBdanielBcrossB	contrivedBcombinationBbelievesB810BtravelBstronglyBrobinBreviewerBproduceBpracticallyB	painfullyBhearingBforeignBfootB
flashbacksBfailureBdestroyBdesertBassumeBukBsheriffBrelateBlargelyBgoldenBdevilBchooseBbrownBtapeB	sufferingBstoppedBsequelsBreliefBrangeBlosingBjenniferB
gratuitousBfeetBclueBattitudeBvictorBtrekBsellBrescueBprotagonistBproperBmattersBlakeBhitlerBendlessBdrewBsingerB
portrayingBpassedBidentityBhillBgrowBgermanyBcontrastBclassicsBcapableBbearBbarbaraBauthorBwoodyBtwentyBtrainingBthoughtsBtestBroseBranBnaiveB
generationBextrasB
excitementBdreadfulBbroadwayBvehicleB	teenagersBsistersBruinedB	hopefullyBframeBfootballBchosenBbombBvoteBparodyBmeetingBlouisB
irritatingBinsaneB
individualBexecutedB
depressingBasianBwinnerBunlikelyBrBpostBlowerB	halloweenBfareBannBancientB	treatmentBtalksBsimonBproductBnativeBluckyBfillBanimeBwalksBtheatersBsportsBroundBmarryBjosephBheckBgoryBfactsBeatingB	creaturesB
cinderellaBbaseballBtediousBsympatheticBrainBlevelsBlearnsBexcitedBdryBcostumeBcleanBchiefBanneBwideBresearchBportraysBlawyerBinsultBhorriblyB	describedB
commercialBbodiesB
amateurishBafricaBtitanicBtendBscareBemotionallyBdepictedBcrowdBboatB50sB	substanceBsoftB
satisfyingBfreddyBfitsBcaresBsonsBpromiseBinvolvesBhowardBgrownBfoolBdogsBdangerBcloserBchickBcartoonsBcapturesB11BtinyBsupernaturalBstudiosBservesBseatB	qualitiesBlukeBhunterBhauntedBhandsomeBgordonBflyBexploitationBevidenceBchoseBcategoryBpriestBmindsBmaxBguiltyBclaimBbringingBasleepBanglesB1970sBwalterBvoicesBunitedBtillBkickBeuropeB710BwelcomeBwarmBsuffersBsendB
relativelyB	offensiveBnorthBmodelBlewisBjackieBinitialBfleshB
disgustingBcircumstancesBassB	appealingBweeksBweekendBtrilogyBtargetBstorytellingBshakespeareBsaturdayBroutineBrentalBpityBobsessedBmattBhauntingBhalfwayBgrantedBenemyBcashBshockedBreporterBrepeatedB	promisingB
previouslyBmaskB
mainstreamBhangingBdragBcostBappropriateBsafeBrogerBremainBrecallBnancyBhideBfameBexperiencesBeightBdeathsBconvinceBcanadianBangelBamateurB	virtuallyBvictoriaBtouchesBoliverBlynchBlatestBdeadlyBclaimsB
adventuresBaccidentallyBwayneBstealsBsourceBsBryanBpresentsB
overthetopBfocusedBfactorBdrawBcorrectB	convincedBcolumboBaliensBuniverseBunfortunateB	surprisesB
strugglingBspeechBsharpBrobotB	professorBhandledBdubbedBcoveredBcoreB
continuityBcombinedBwalkedBsurrealB	structureBruinBroyBholdingBfranceBbugsBaccentsBspeaksBserviceBrevealBmovementBmarieB	highlightBhallBcolorsBbreakingB1980sBwouldveBtwistedBremotelyBpullsBpsychoBprovidedBpileBmistakesB	melodramaB	invisibleBinnerBenterBdesignedBdegreeBcontemporaryBtreasureBteenagerBsinatraBsecurityBmassiveBmarketBjunkBidiotBhatBharshBviewedBringsBprincessBpowellBplansBfalseBfB	downrightBbotheredBblowBbirthBartistsBveteranBteensBpoliticsB	performedB
occasionalBlesbianBkindsBirishBfrighteningBfridayBexpressBexplainsB
everywhereBedwardBdropBdisplayBconversationBchangingB	atrociousBwhoeverBuninterestingB
refreshingB	recognizeB
propagandaBpathBmultipleBlousyBfairyBdollarsBvietnamBunconvincingBsuspensefulBspendsBprimeBpaperB	nominatedB	narrationBmexicanBlonelyB	influenceBhuntBforgettableB	favoritesBdirectlyB
departmentBdeliveryB	childrensB
australianBangerB	subtitlesBrussellB	legendaryBforestBemmaBdeeperBdarknessB	committedBbraveBbelievedBandersonBwitnessBtheoryBsympathyB	statementBspyBpriorBpaintBnetworkBmgmBmatrixBhongBghostsBfuBeffectivelyB
worthwhileBwilsonB	universalBscaresBrequiredBpeaceBmagicalBhedBgraveBfiguresB	executionBaliceBsurfaceBsundayBofferedBmothersBmooreBlifetimeBjuliaBjobsBfiguredB	fictionalBfeaturedBexamplesBdemonsBandorB13BtrappedBsleepingBsectionBmexicoBjonBjBexperiencedBamazedB25B1930sBweaponsBvonB
thankfullyBsummaryBregretBprintB	listeningBimageryBhiredBdonaldBdesperatelyBclarkB
californiaBwearBvarietyBtexasBskinBroughBpittBlearningBkimB	interviewBextentBexactBcrudeBcrashBcowboyBburnsBafricanBachieveBsuckBscaleB	ludicrousB	happinessBforthBdeviceBcuttingBbmovieBaccountBabuseB14B110BusaBreminiscentBrealizesBrachelBquestBproductionsBpassingBnudeBnovelsBmountainBmatureBheavilyBfreemanB
expressionBeroticBdriverBcriticalB	criminalsBcourtBblahBsoundedBnecessarilyBlightsBintellectualBfourthBdozenBclichédBcaineBblockbusterB	abandonedB
understoodBstayedBsortsBsirBseekBregardBpayingBpacinoBpacedBmariaBjulieBinsightBgrimBgasBfunnierBforgotBfocusesBcodeBchinaBbusBbelovedBbeliefBangleBaintBteethBtedBtechnicallyBsignificantBripBrevealsBrawBplacedB	encounterBdragonBviewsBtableBsucceedsBstolenB	slapstickBskillBsettingsBscenarioBsarahBrelatedBpregnantBpreferBmusicalsBmildlyBlucyBinspirationBignoreBfriendlyBfacialBexpertBenvironmentB	entertainB	disbeliefB	depictionBdeanBcarryingBbaseB310B
revolutionBlazyBkissBhelpingB	flashbackBcryingBclosingBattacksBangelsBurbanBundergroundBtouchedB	strangelyBstaysBrightsBpraiseBgrittyBgreaterBfabulousBextraordinaryB	elizabethBdrivenBconveyBcomicalBboreBanswersBwarnedBviaBtrickBsusanBstopsBspiteBrogersBpositionBnonexistentBformatBfaithfulBcarriesBcaringBbuyingBbreathtakingB	amazinglyBalrightBwinsBstarredBserveB	sensitiveBratingsBmurdererBlugosiBlieBkungBfallenBdecadesBarmsBamongstBterrorBtaskBsubplotBstereotypicalBsitcomBsidneyBshadowBsanBraisedBproudBoscarsBembarrassedBdudeBcruelBcontainBcampyBcallingB410BwesternsBsinisterB
reputationBproperlyBnavyBmetalBlockedBjoinBinternetBindiaBgraphicsBfbiBdescriptionBbreathB	traditionBtalesBruleBrollingBprotectBnowadaysBmurphyBlauraBlarryBeverydayBdailyBcouldveBcomplicatedBcellBbusyBblairBbeastBwearsBwarnerBupsetBsunBsufferBstanleyBrentingBpicksB	notoriousBnedBmanagerBjailBironicBgoofyBdemonBbridgeBbradBbeachBbasisBappreciatedB	afternoonBxBvacationBturkeyBronBrivalBraiseBquirkyBpurelyBpreparedB	obsessionB	marvelousBleslieBjungleB	inspectorBineptBhorrificBhenceBgodsBflightB
experimentBdrinkingB	criticismB	challengeBashamedB2000BwriterdirectorBwaveBtimingBstatusBriskBpunchBprotagonistsBpresentationBnotableBmereBhanksBfosterBenjoyingBculturalBchillingBcausesBblownBtitlesBswordBstBscriptsBoriginalityBlaneBguardBgrossBglimpseBdislikeB	destroyedBchoicesBbourneBauntBattackedB910B2006BwwiiBtruckBtoyBthrowsBthrowingBsoldBsocalledBservedBpetBnonethelessBjohnsonB
interviewsB	innocenceBhonorBdennisBdelightBcrisisBcasesBcabinBbbcBarrivesBtermB	stupidityBstomachBspoofBremindB	regardingB	referenceBopenedBmadnessBgloryBchargeBcarriedBbalanceBwastingBtracyBtopicB	thrillingBshutBretardedB	obnoxiousBmirrorB
meaningfulBlyingB	initiallyBindieBhaBguessingBgruesomeB	enjoymentBdrivesBdressBcurseB	authenticBandrewsBaddingBtreeBtenseB
sutherlandBsuccessfullyB	strugglesBsouthernBracistBovercomeBmichelleBleagueB	hitchcockB	franchiseBflowBcornerBtightB
scientistsBlionBlesserBjessicaBidioticB
frequentlyBflawedBclipsBbitterBtextBstylishBsingsBrobertsBremoteBrefusesBpoignantBnineBlegsB
intentionsBhusbandsBhoodBhintBhardyBglassBfortunatelyBbuckBbrokeBwishesBwarnBsullivanBshowerB	screeningBripoffBreplacedB	ourselvesBmassBjumpsBintroductionBinterpretationB	essentialBescapesBdinnerBcontroversialB	bollywoodBbettieBalbeitBweaponB	techniqueBstrangerBsidesBseekingB	searchingBracismBneighborhoodBneedlessBnationBmouseBmillionsBmidnightBmentallyBhundredsBexistsBeastBcynicalBclaireBbatBatmosphericBairedBuncomfortableBtornB	thousandsBsuspectsBstormBsinBshortsBrocksBportraitBpersonalitiesBoddlyBmonthBkurtBcontactB	connectedBcomedianBzoneBuBstepsBspotsBseparateBridingBreedBproofBmiscastBhandleBexpressionsBegBcredibleBcookBcameosBsuggestsBsucceedBshinesBshedBrevolvesBreunionBplasticB
physicallyBphilipBperformBpatientBpatBlucasBlessonsBjayB
incoherentBhelenBdollarBchristBbagB
attemptingB80BtroubledBtriteB	thrillersBspokenBsnowBmansionBlovableBjewishBhappilyBhamletBdearB	competentBbrooksB	advantageBwidmarkBtrialBsufferedBsavesBrepeatBprideBhorsesBhidingBfactoryBdevelopsB
determinedBcomicsBachievedBwomansBwhereasBwealthyBthiefB	sexualityBpackBobjectBoBnotedBmillerBholidayBhillsBgreatlyBfortuneBcrimesBcheeseBcatsBbelongsBacceptedB1960sBtuneBsumBslightBralphBparBnelsonBlibraryBhundredBhostBgrippingBensembleBdubbingBdorothyBdigBdareBcourageB	confusionBworryBwallsB
terrifyingB
techniquesBteachBstealingB	rochesterBpersonsBnoseBnobleBnaziBmobB	intensityBhuntingBgutsB	godfatherB	endearingBcolorfulBcloselyBcheckingB	cardboardBboredomBbetteB
attractionBattachedBannoyedBandrewB30sBtributeBstretchBstoodBspikeBsleazyBsegmentsBscreenwriterBplagueB	onelinersBmindlessBmassacreBlisaBlaidBhomelessBhoffmanB	expensiveBcubeBconsistsBchasingBchasesB	believingBappearancesBadaptedBvideosBunintentionallyBtBshootsB	reactionsB
perfectionBnBlloydBironyBhBgainBgagBfishBestablishedBentryBeasierBdraggedBdialogsBdenzelBcredibilityBtricksBtourBtiedBstrikingBsandlerB
redemptionBpoolBoilBluckilyBinfamousBianBholeBflawBexceptionalB	countriesBcooperB	complaintBcharismaticBboundBuselessBsubplotsBshapeBsetupBsentimentalBrippedB
performersBpaintingBnotchBmst3kBmedicalBkingsBjeremyBimaginativeBhorrorsBheartsBfxBdramasBcivilBcatholicBburtBbirthdayBbearsB	attractedBalasB
afterwardsB18BwakeBunforgettableBstuntsBquoteBpieBmatthauBknifeB	intentionBessenceB
encountersBdealtBcoversB	continuedBconcertBconcernsBchairBcardBbranaghBarmB	appearingB
acceptableBvirginBsurroundingBstanwyckBsendsB
revelationBrelevantBprofoundBnightsBneatB
miniseriesBkarloffBhangBgreyBgoalBequalBeerieBdisagreeBdentistBdancerBcolourBcharismaB	carefullyBboB2001BwannaBshallBreynoldsBpleasedBpitchBnuclearBloserBjakeBindividualsBiiiBhudsonBgothicB
frustratedBcontractBchuckBchanBagedB1stBwonderedBvincentBstrongerBstrictlyBseagalBprojectsBneckBmagazineBletterBitalyB	insultingBhuhBhopedBhandfulBguestBgloverBfiredBdroppedBdawnBcurtisB	countlessBcorruptB	conditionBburiedBbucksBbrandBbeingsBbeatingB	assistantB40sB17BwinterBwarriorBunionBstrikesBspendingBsilenceBrapedBneighborBmenacingBmarksBlosBlistedBlincolnBlettingBkicksBfittingBcanadaBborderBbollBbattlesBbasementB	attemptedB24BweightBvirusB
universityBtradeBthrillsBthousandB	territoryB
surroundedBsophisticatedBsomeonesBsilverBshiningBoccursBlackedBkoreanB	intriguedBgrowsBgiftBfifteenBfacedBevaB	elsewhereBdoorsBcharacterizationBchancesB	catherineBblowsB	appallingBaccusedBworkersBsticksBspecificBrowBrelativeBreallifeBpsBpointedBmustseeBinvestigationBimportantlyB
importanceBhookedBhomageBhipBhealthBgottaBflawlessBflashBdocumentariesBcompetitionBcloseupsBcamerasBbulletsBboxingBbobbyB
associatedB	ambitiousB210B	worthlessB
uninspiredBtheydBrushedB
resolutionBrequiresBpushedB
overlookedBnazisBmileBmediumBkillingsBkenBignoredBfayBemBdukeBdrinkBdestructionBcreatorsBcraftedB
conspiracyBconsequencesBallowingBaforementionedBadamsBachievementB3dB2005BtrapBtonsBtheyllBtalkedBsuperblyBstatedBspellBsoulsBsolveB	sacrificeBprogressB
outrageousBnotablyBkaneB	graduallyBgenerousBevidentBempireBelvisB
discussionB	curiosityBcommitBcoleBcheBwickedBwatersBwalkenBvagueB	typicallyBtoiletBstylesBsplitBshortlyBshadowsBrobotsBrealiseB
presumablyBpressBpersonaBperBleB	kidnappedB
inevitableB
horrendousBhireB
highlightsBhestonBgrayBdrawsBdiseaseBderekBcupBcousinBcarolBcaredBbritainB
brillianceBbarryBwondersBwardBthumbsBsunshineBstanBspoiledBsexuallyBpreciousBpianoBpartlyBmildBmessagesBlargerBidentifyBgenresBfatalBdragsBdisplaysB	dinosaursBburningBbrieflyBbrandoBbeerBalikeBwingBwannabeBunbelievablyBsuitsBstrikeBsoleB	revealingB
representsB
regardlessBplanningBnurseBnotesB
motivationBmeatBjumpingBidiotsBgoldbergBflynnBfearsBexerciseBelderlyBdocBcraftB
convolutedB
cameraworkBatlantisBareasB911BwatchesBtimelessBticketBstuntBshyBsallyB	returningBresponseB
reasonablyBpreventB
performingBopposedBnoiseBmelodramaticBmafiaBlabB	inspiringBimprovedB	franciscoBestateB
depressionBcameronBbrideBbeatsB	alexanderB45BwesBthugsB
subsequentBspringBsmoothBsloppyB	secretaryBsavageBsadisticBrushB
repetitiveB	remainingBpushB	overratedBmoralityB	miserablyBknockBkapoorBjerkB	instantlyBincreasinglyBhatredBguiltBgrandmotherBfarceBexplicitBdvdsBdrivelBdiamondBdanesBcluesBburnBastaireBarrestedBargumentBargueBaimedBabsenceBtwelveBtreesBthreatBsynopsisB	symbolismBsplendidB	spiritualBspinB
scientificBrapB	providingBpovertyBposterBpickingBphotographedBoverdoneBoccurredBminimalBmidBlogicalBindiansBhypeBhittingBgadgetBentersBemilyBdrawingBdoctorsBdirectorialBdarkerBcruiseBcitizenB	carpenterBcaptivatingBburnedBbleakBaffectedB
tremendousBsubjectsBrootBnonB	mountainsBmelBmatthewB	landscapeBkudosBjuvenileB
ironicallyBintentBgroupsBfancyBfailingBdraculaBdistanceBcriedBblankBbeatenBadmireBadequateBwreckBwarrenB
unbearableBturnerBtorturedBtimothyBtallBstiffBsmokeB	slightestBscreamsBrichardsB
restaurantB	resemblesB
repeatedlyB
reasonableBreactBpushingBpurpleBpsychiatristBproceedingsB
overactingBoughtBmonkeysBlikingBkitchenBkennethBjealousBinvolvementBgreekB	forbiddenB	elaborateBdireBcoxBcomplainBbrainsBbellB	australiaBagingBadBthroatBsuperficialBstruckBshellBscriptedBsadnessBritterBreturnedB	representBreachedB	producingBpackedBoverwhelmingBofficialBmitchellBliberalBlegBjeffreyBhousesBguideBfuneralBfeverBfarmBexploreB	executiveBdignityBdevotedB	currentlyBcubaBclothingBburtonB	broadcastBblendBbibleBbakerBarnoldBalltimeBwintersBtommyBthrewBteaBspiritsBsovietBreliesBranksB	psychoticBperryBofficersBobscureBneilBmodestyBmastersB	manhattanBmachinesBhatesBhardcoreBgentleBforgiveBextendedBexistedBdistantB	decisionsBcraigBconversationsB
concerningBchildishBbrosnanBbrazilBbathroomB
admittedlyB	yesterdayBwebsiteBunintentionalBtunesBthrillBtempleBslapBsignsBsellingBselfishBreducedBramboBoccurB	movementsBmissesBmeritBmeasureBlooselyBloadBinstallmentBincidentBimpressBimaginedB
hystericalBhurtsBhughBholmesBheroicBharderBglennBgandhiBformsBedieB	eccentricBdrunkenBdozensB	discoveryBdigitalB	dialoguesBdemandsBdefiniteBcorpseBcomfortableBciaBblobBaudioBarriveB
altogetherBviciousBupperBtwinBtranslationBthreateningBswedishBswearBsurfingBstoleBsizeBsamuraiBresemblanceBreceiveBordersBlilyBjamieBintrigueB
innovativeBhorridBhighestBheatB
futuristicBexploredBdynamicBdistractingBdancersBcreationBchoreographyBcaryBcagneyBbreastsBaccomplishedB20thB2003BworriedBvividBtriumphBthirtyB	survivorsB	superheroBsecondlyBpullingBpossibilitiesBpopcornBphotographerBpetersBofferingBnormanBmeltingBmakerBmaggieBkiddingBimplausibleBgardenBfalkB
explainingBexBescapedB
enterpriseBenormousBenjoysBelviraBeditorBduoBdetailedBdancesBcommercialsBchaplinBcatchesBcarreyBbuddiesBbroadBbradyBbeneathBaprilB2004B2002ByellowBwolfBwivesBwebBwBvastBunwatchableBunfoldsBtransformationBthickBtameBstripBstayingBspookyBsmokingBshipsBsecretsBremovedBpretendB	possessedBpassesB
nightmaresB
journalistBirelandBinvestigateBhollowBfolkBfocusingBfloridaBfighterBexaggeratedBeveBeaseBdifferencesBdestinyB	describesBdeliberatelyBcouplesBcontraryBchainBcarterBcarlBbuildsBbrooklynBbiteBanywaysBanticsBadvanceBwealthBupsBunevenBtrioBtitledBstillerBstagedBshineB	seventiesBruralB	realizingBpromBpossibilityB
pleasantlyB	nostalgicBlaurelBjetBinvolveBgenericBgBfisherBetBdiscussB
developingBconnectBcombatBchasedBboldBbannedBbackgroundsBapesB16B
unpleasantBtoysBtoddBtiesBstringBsoloBsnlB	satisfiedBsagaB
recognizedBpulpBpreviewBpanicBoddsB	neighborsBlightheartedBlatelyBkingdomBkarenB	inventiveB
introducesB	illogicalBholyBhitmanBfurthermoreB	explosionBexoticBendingsBdoomedBcusackBcgBcareersBblewBblatantBbenefitBbedroomBbackdropBavoidedBamyBwidowBtenderBstressBstaffB	spielbergBshoesBruthBprimaryB
populationBpaysBnotionBmonkeyBmodelsBmenaceB	madefortvBlolB
hollywoodsBengagedBdianeBdevoidBdamageBconsistentlyBcoherentBchorusBbluesBbinBarrogantBangelesBahB60BurgeBtravelsBtoplessBsportBsidekickBschemeBruthlessBridiculouslyBreachingB
philosophyBnearbyB	murderousBmickeyBmethodBlipsBjonathanBhollyBhammerBguessedBflopBfliesBexposedBemphasisBdollBdivorceB	disturbedBdisappearedB	depressedBdefeatBconventionalBcomposedBchaosBcausingBbirdsBbirdBbandsB
artificialBannieB1990sB0ByellingBtracksBtapBsoccerBshelfBsharkBsettleBsatanBroomsBquotesB	performerBpagesBoutcomeBonscreenBoccasionB	newspaperBmotivesB	miserableBmeaninglessB	lifestyleBjudyBjesseBinstantBinferiorBillegalBignorantBidealBhalB	gangstersBfrustrationBdropsB	displayedBdaringBcrushBcountrysideB	containedBcoffeeBclintBcheatingB	charlotteBbushBadorableBabusiveB2ndBversusBunderstandableBtonightBterryBstinksBsnakeBsmallerBscopeBsafetyBromanB
remarkablyB	purchasedBpropsB	primarilyBphotosBonedimensionalB	mysteriesBmoodyBminiBlikewiseBhartBgrabBgloriousBendureB
disjointedBdiscBdesignsB	dedicatedB	companionB	commentedBclosetBalertByaBwedBwalkerBuserB	travelingBtrailersBtadBsubtletyB	streisandBstinkerBstellarBslaveB
simplisticB	similarlyBsellersBridBrebelBrageBpurchaseBpoliticallyBpigBparentBoffendedBmixtureBmildredBlyricsBjustifyBjudgingBhuntersBhonestyBhintsBhboBfondBeyreB
explosionsBerrorsBengageBegoBdubBdownhillBdatingBclumsyBbuffsBbuffBblockBaffordBabcB1996BweakestB	wanderingBuweB
unsettlingBunhappyBunderstatedBthruBswitchBsurvivedBsuitedBsimilaritiesBrepresentedBreportB	provokingBpromisedBprequelBpaintedBouterBnicholasBmotivationsBmontageBlouBleonardBlayBkirkBisolatedBhideousBheartwarmingBheadedBgraspBgoodnessBgodzillaBgingerBfreakBearnedBdutchB
corruptionBconsiderableB	comparingBclaimedBcircleBcentersBbuttBbottleBbonusBblacksBaweBakshayBaidBagentsB70B1972BwillisBtroopsBtransferBswimmingBsurvivalB	succeededBstreepBsteelBsentenceB	senselessBrivetingBremarksB	relationsBrecordedBreachesB	principalB
passionateBorsonB
nominationBnervousBmyersBmargaretBjumpedBinteractionB
influencedBimproveBgrandfatherB	formulaicBfingerBfaultsBellenBeBdisneysBdirectsBdesperationBdaviesBdaddyB	corporateBconvincinglyBcinematographerB	celebrityBaffectB	abilitiesB3rdB1999BwomensBwishedBwallaceBvirginiaB	vengeanceBtrashyBtongueBsubBsplatterBspareBsitsBrobbinsBrankBoutfitBnarratorBmistakenBmarioBmarchBlinkBlawrenceBkhanBjoeyBinvasionBingredientsB
horrifyingBenemiesBearsB
disappointB	disappearB
deliveringB
consistentBcolonelBcliffBcleverlyBchicagoBchestBcaveBcardsBcampbellB
basketballBadvancedBwaitedB
vulnerableBvoightBunawareBtearBsuitableBstiltedBstevensBstaringBsolelyBscottishBscoresBrocketBrevolutionaryB
prostituteBpornoBplantBozBmatchesBlustBlivelyBlindaBlimitsBleesBlatinBlastedBitllBincomprehensibleB	immenselyBhungBhumbleBhamiltonBgrinchBfeedB	excessiveBdollsBcriticBcommandBcoachBclosestBcheatedB	celluloidBbulletBarrivedBamandaBaccompaniedB2007BwindsBtiresomeBspreadB	slaughterB
simplicityBsappyBquestionableBplightBphilosophicalBniroBmiikeB	macarthurBkickedBiranBinaneBhookBhomerBhandedBgialloBfrancisBfloatingBfelixBexplorationBearlBdreckBdonnaB
destroyingBcureBcrackBclassesBcitiesBchallengingB	attitudesBarguablyB	alongsideBagreesBagreedBwavesBvBtieB	terroristBspecificallyBsimmonsBshirleyBshakeBrickBresortBracialBpracticeB	operationBnamelyBmontanaBmiseryBmiracleBmethodsBmarsBmaintainBloadsBleoBlengthyBjazzB	everyonesBdimensionalBderBdefendBdaveBconB
complexityB	communistBcatchyBcakeBbakshiBawakeBappreciationBamountsBadviseBadvertisingBabysmalB1980BwrappedBworeBvaguelyBundoubtedlyBshowdownBshirtBsecretlyBrexBrelationBreflectBraisingBportionBpoetryBoverlookB	nicholsonBmundaneBmonkBmartyBmBlemmonB
lacklusterBjuneBiraqBhankBfuryB	equipmentBembarrassmentB	educationBeatenBeagerBdoubtsBdolphBdinosaurB
definitionBconstructionB	conflictsBconcernBchessBchannelsBcalmBbuttonB	buildingsBborrowedBblowingBblakeBbeattyBbathB	alternateB1983B	wrestlingBwoundBwisdomBwillieBwarriorsBvisibleBtreatsBtimonB
thoughtfulBtaxiBtaughtBstonesB
stereotypeBsincereBsimpsonBsignedBsidBshowcaseBshelleyBroyalBrootsBrisingBresponsibilityBrecognitionBratsBpsychicB
progressesB
pretendingBpotentiallyBphantomBparallelBnationsBlBkennedyBkayBkarlBjoinedBincompetentBharveyBfulciBfooledBfirstlyB	financialBexchangeBempathyBedgarBdominoBdevilsBdaltonBconfrontationB	compelledBcombineBcivilizationB
christiansBbrutallyBbrendaBarrivalBarmedBamericasBairplaneBaidsBaccessBwritesBwetB	voiceoverB	verhoevenBusefulBtroublesBtrainedBtigerB
suspiciousBsueBstaleBspeciesBshawBsevereBrockyBreadsBrabbitBpotBopinionsBolivierBnutsBmummyBlettersBjustinBjuniorBjennyBintentionallyB	integrityBheightsBfrequentBfoulBfixBfasterB
fascinatedBfacingBensuesBelmBelephantB	dimensionBdickensBdefinedB	crocodileB
creativityBconservativeBclownBclosedBclicheBchicksBbladeBbelaBbareBaustinBairportBwoundedBwizardBwishingBwarmthBvisitsBunitB
underneathBtwilightBtravestyBtagBstoogesBspiritedBsopranosBscotlandBruinsBrubyBrobinsonBremakesB	relativesBrandomlyBpressureBpolishedBpoeticBpoemBplannedBpitBnycB	nostalgiaBmollyBmightyBmasterpiecesB	marketingBmadonnaBmacyBloyalBlowestBlosersBloadedB
literatureBlawsBjulianBironBinventedB	imitationBhilariouslyB	guaranteeBgermansB	fashionedBexposureBelegantBeightiesBeastwoodBdarrenBcrucialBcopiesBconstructedBcomposerBchapterB	carradineBbluntBbeliefsBbangBangelaBacidB1990B1940sBvegasB	upliftingB
terroristsBtechnicolorB	survivingBspokeB	septemberBrottenB	renditionBreceivesBratBquitB
punishmentBpierceBpeakBpartiesBpantsBonlineBmoronicBmistressBmetaphorBmerylB
mentioningBlitBkyleB	introduceBinteractionsBillnessBhopperB
helicopterBheartbreakingBgregBgreedyBgatherBgarboBfirmBexceptionallyBethanBdutyBdepictsBdanielsBdaB	climacticBclausBchristyBbootBblondBballsBassaultBzombiBwhoopiBwellsBviewingsBvaluableB
unoriginalBtubeBtribeBsuspendBsufficeBsoundingBsmilingBsissyBsinkBsimultaneouslyBsignificanceBschoolsB
richardsonBpunkBpromisesBprisonerBpopsB	plausibleBphonyBperformsBpalaceBnonsensicalBnicoleB
middleagedB	masterfulBmallBloneBinappropriateB
homosexualBhistoricallyBgraysonB	generatedBeugeneBeducationalBdistinctBdislikedB
directionsBdementedBdamonBcravenB
confidenceBcoastBchoreographedBchoosesBchampionshipBcdBcapitalBbelongBbehaveBbaldwinB	authorityBattendBassignedB3000BworkerBwitchesB	wellknownBvoicedB
transitionB	testamentB	supportedBsquareB	shouldersBsharonBscroogeBrothBresistB	primitiveBpackageBopportunitiesBnonstopBminimumB
mechanicalBitemsBisraelBintimateB	interestsB	inabilityBhopelessBharmBfondaBflairBfidoBfascinationBfamilysB
equivalentBeditionBedgyBdustBdrearyBdoomBdixonBdistributionBdesiredB
complaintsBcanyonBbutlerBbumblingBballetBbabeBawhileBashleyBantwoneBamitabhBalecB	alcoholicB
additionalB
accomplishB	witnessedBwidelyBustinovB	unlikableBtastesBstunnedBsolutionBsneakBslickBskullBsharedBsentinelBsabrinaBrobberyB	resourcesBresidentBregionBraymondBrangersBpursuitBpolanskiBpettyBpatienceBpanBoverlongBmodeBmateBknockedBkickingBjoinsB
inevitablyBhydeBhopkinsBhersB	hackneyedBgreedBfoughtB	expressedBdomesticBcreatorBcontestB	classicalBcinemasBbridgetBbikoBberlinBattorneyB	ambiguousBwackyBunpredictableBunexpectedlyBteachingBseeksBrudeBrubberBrisesBrestoredBreidBratesBrandyBpreviewsBpg13BpamelaBnewlyBnailBmilkBmessedBmasksBlundgrenBkeithBinexplicablyB	greatnessBgerardBgenerationsBfunctionBframedBfestBfavourBelBearBdespairBdesertedBdescentB	depictingBcringeB	correctlyB
compassionB	companiesBcarrieBcarefulBbusinessmanBbullBbudB	brutalityBboneB	biographyBbacallBadaptationsBacceptsBabusedB1971B13thByardB
wildernessBvalleyBtomatoesBteachersBsurvivorB	subjectedB	strangersBshanghaiB	sentimentBrequireBreplaceBreleasesB	recordingB	prisonersB	pricelessBprestonB
presentingB	preciselyBpatientsBpassableBpalBoceanBnerdB	musiciansB	murderingBmorrisB
misleadingBmentionsBmanipulativeBliBlenaBlegalBlegacyB	laughablyBlandingB
irrelevantBinstinctB	immediateBiconB	heartfeltB
frightenedBfiBfeelgoodBexperimentsBexaminationB	evidentlyB
enthusiasmBelsesBdroveBderangedBdemandBdeceasedBdanaBdammeBconneryB
conditionsB	conceivedB	comediansBblastBbernardB	behaviourBbargainBaspiringBanalysisBamusedBaltmanBaliciaB1973B	witnessesBwifesBwellwrittenBwakesBwaitressBviceBvanceBunrealBuncutBtrailBtownsBtopnotchBtoothBtankBsumsBsubparB	stretchedBstraightforwardBshouldveBseverelyBscreensBsciBroommateBretiredBrespectivelyB
respectiveB
reflectionBraveBraisesBrainesBpunBpearlBoldestBnathanBmuseumBmorbidBmannBmaniacBiconicBhelloBheightBhearsBgrabsBgiftedBfrankensteinB
expeditionBdysfunctionalBdudBdressesBdianaBdesiresBdefenseBdawsonBcloseupBcitizensBchristianityBcheckedBcannonBcannibalBassassinBalcoholBaccuracyB	acclaimedBabrahamBwhollyBwendyBunfairB
underlyingBtripeB	trademarkBtrackingBtendsBtackyBswingB	suggestedBstuartBstarkBspringerBspaceyBsoxBshakyBshadesBsatisfyBrussiaBremadeBrefusedBrecordsB	receivingBpurposesB	profanityBproceedsBprizeBpreposterousBprepareB
popularityBpoisonBphotoBpaulieBpalanceBorderedBobjectsBmtvBmatchedBmarshallBmankindBloyaltyBliteraryBlandsBkittyBkathrynBkansasBjoshB
indicationBimprovementBgundamBglowingB	gentlemanBfilthBfedBestherBeasternBdrakeBdinBdeniroBconventionsBconfessBcomfortBclipB
challengesB	capturingB	barrymoreBassumingBalternativeBaimBagendaB	affectionB1997B1993BwalshBvulgarBunfoldBtylerBtomorrowBsungBstumbledBstudyingBsparkBspainBsoonerBsliceBsirkB	sillinessBscoobyB	scarecrowBromeroBritaB	resultingB	respectedBrelaxBreferredBreevesBreelBrecycledBrecommendationBrajBpuppetB	policemanBpinkBpartnersBparsonsBpacificB	objectiveBnaughtyBmeritsBmayorBmapBmaidBlaurenceB
landscapesBkubrickBkissingBguestsB	grotesqueBgrahamBfingersBfieldsBfemalesBexperimentalBemperorBdisorderBdifficultiesBdesignerBdemonstratesBcrystalBcrawfordBcostarsBcoincidenceB
challengedBbuffaloBbikeBbesideBbayBawfullyBauthorsBastonishingBapeB	admirableB	absurdityB35B1987B1984B1978BwweBwrapBweaknessBwangBunseenBsixtiesBshtBshoddyBsharesBsensesBroofB	reluctantBquinnBpromoteBphillipsBparanoiaB	paintingsBnooneBnoisesBninjaBnephewBmelissaBmassesBkumarB	insuranceB
imaginableB	ignoranceBhughesBguinnessBglobalBgemsBfrustratingBelectricBeditBdemiseBdatesBcountsBcostarBcorpsesBcopeBcoBchargedBcampaignBburstBboomBbeatlesBassumedBalbumBadoptedB
activitiesB75B1968BwildlyBwelldoneBweatherBvitalB
translatedBtowersBstoresBstandupBsingersBseldomBseedBschlockBsandraBrooneyBrompBrodBriotBresembleBrealmBreadersBpropertyBproBparadeBpaltrowBoutingBoutfitsBomenBollieBmuslimBmusicianBmuddledBmorallyBminusBmaloneBlouiseBlockBlocalsBliftedBjawsBinsipidBhulkB	householdB
hopelesslyBhealthyBharmlessBhaplessB
guaranteedBglassesBfreaksBfogB	exploringBdiscoveringB
difficultyBdelBdeafBcreepB
conscienceBconceptsBchoppyBchillsBbrosBbrendanBbergmanBbasingerBbaconBantsBambitionBalfredBaffairsBaddressB
acceptanceBabsentB1995B1933B
widescreenBvictoryBtriangleBtierneyBthoughtprovokingBteachesBswimBsorelyBshortcomingsB	selectionBregardedBreferBrecognizableBprovocativeBpreyB	prejudiceBphilB	paramountBownersBnicolasBmurrayBmuppetsBmoreoverBmolB	mentalityBlesBleonBkungfuBkolchakBinsistsB
insightfulBindependenceBharrisonBhandlingBgregoryBginaBfrancoBfillingBfemmeBfeministB
expositionBexploresBeternalBcreditedBcreamBcrazedBcoupB
convictionBcomparisonsB
commentingBclanBcelebrationBbilledBbeverlyBbettyB	befriendsBantonBanilBabruptB2008B19thB1986ByetiBwretchedBwouldbeBvotesBvibrantB	unrelatedBtunnelBtrendBtokenBteamsBstargateBsoftcoreBslimyBshoutingBshoppingB	secondaryBrolledBrhythmB	repeatingBrenaissanceBrelyBreignB	prominentBpremiereBpostedB
portrayalsBpitifulBpaxtonBorleansBnervesBnativesBmclaglenBlushB	knightleyBknightBiqBinfectedBhiresBheadingBhammyBguitarBgriffithBgenieBgamblingBfontaineBfillerBdifferentlyBdesBdeeBdanishBchampionBchainsawBcatchingBcastsBcassidyBcaricaturesBcancerB	camcorderBbridgesBbondageBbendBbabiesBaudreyBartworkBapplaudBaceB1936ByearoldBwindowsBvisitingBvisitedB
underworldBtwinsBthreadB	terrifiedBtapesB
suspensionBsunnyB	strongestBsquadBspinalB	speciallyBsoupBsnakesB	sincerelyBservantBsassyBrukhBrenderedBquaidBpostersB
positivelyBphysicsBphraseBpennyB	partiallyBparadiseBoptionBnodBnemesisBnatalieBmillBmichaelsBmegBlumetBinvitedBinhabitantsBinexplicableBhkBhangsB	glamorousBgabrielBfuriousBfiftyBfeedingBfarrellB	fantasiesBenteringBdukesBdoseBdevicesB	deservingBdeletedBcrueltyBcoupledBcouchBconcludeBcomplainingBcommunicateBclockBcircusBchildsBcaliberBbentBautomaticallyBattractB	amusementB
adolescentB
accuratelyB1989B1988BByoungestBvotedBventureBvainBunexplainedBundeadBtriviaBtripleBsuitablyBstumblesBstareBscreenwritersBscariestBrossBrerunsBrepresentationBremainedBrejectedBrefuseBreflectsBredeemBramonesBpraisedBpokerBpgB
passengersBorangeBnewerB	misguidedBmirrorsBmarvelBlindsayBlanceBklineBivBinvitesBinvestigatingB	ingeniousBinfoB
industrialBimmatureBhomicideBhokeyBheelsBguardsBgenderBgearBfullerBfortyBfiftiesBfeatBexcellentlyBemployedBelevenB	disgustedBdisguiseB	departureBdelicateBdebateBcueBconveysB
controlledB	confidentBbulkBboastsBbiasedBbelushiBauthoritiesBasylumBalteredBactiveBwhaleBweakerBvoyageBveinBunsuspectingBtemptedB	tastelessBsymbolicBsweptB
suggestionBstandoutBstairsBsmilesBscrewedBremoveBrealisedBquietlyB
principalsBpredecessorBpoleBpokemonB
phenomenonBowenBoutdatedBothelloB	organizedBmitchumBminorityBmillionaireBmayhemBmarriesBmarcBmalesBlocatedBlifelessBlayersBladderBinjuredBinconsistentBhomesBhistoricBhippieBhilarityBhelplessBheistBhartleyBgillianBgereBgateBforcingBfluffB	evolutionB	encourageBdriveinBdressingB
disappearsBdiehardB
detectivesBdestinedB	dependingBdefeatedBdamnedBdamagedBconsiderablyBclaustrophobicBclarkeBcharmsBcasualBboyleBbogusBbetrayalBbeholdBappropriatelyBallensBaddictedB21stB1976B1945B–ByokaiBwingsBwigB
weaknessesBwarmingBwandersBwanderBvisionsB	valentineBusersBumaBtrialsBtrafficBtipBsuburbanBstaticBstackBspreeBsniperBslugsBsleepsBskilledBshoulderBshakespearesBrootingBrippingB
restrainedBresolvedB	residentsBrapistBpredictBoffbeatBnorthernBninaBmutantBmitchBmindedBlifesBjoelBjillBinterestinglyBinsightsBheartedBhawkeBharoldBgrowthBgoodbyeBgatesBgarnerBfadeBethnicBenteredB
engrossingB	endlesslyBemergesBdurationBduckBdroppingBdowneyB	disguisedB
discussingBdependsBdazzlingBdandyBcrackingBcontributionBconnectionsB	computersBclaimingBchickenBcerebralBcentsBcenteredBcarlaB	candidateBbuysBbreedBboxerBbowlBbowBbewareBbanalB	backwardsBattenboroughBanticipationBafricanamericanB
advertisedBabruptlyB73B22B1979B1970BwornBwooBwellmadeBwaxBwatsonBvehiclesBvaderBupdatedB	uniformlyB
undeniablyBtopicsBtokyoBthompsonBtaB
sympathizeBsurgeryB
stunninglyBsteadyBstalloneBstagesBsoughtBsosoBsiblingsBshakingBscoopBscarfaceB	satiricalBromeBridesBreportsBrandolphBpossessB
politicianBphotographsBoutrightBmutualBmuppetBministerBmiamiBmarlonBloisBlinersBlimitBkeenBjulietBjewsBjarringBinternalBinsultsBinspireBincidentallyBhitlersBherosBhelpfulBharrietBhandlesBgypoBgrudgeBgirlfriendsBgimmickBframesBfoolsBfistBfishingBfirmlyBfiancéB	exquisiteBexploitBexcessB
exceptionsBentiretyBdisgustBdetractBdestroysB
describingBculturesBcompeteB	commanderBcluelessBclichesBchuckleBchoosingBcheerBcharltonBcemeteryBbunnyBbtwBbravoBborisBbordersBbeowulfBbanterBartsyBarthouseBarcBanyonesBanyhowBangstBactivityB1994B1969BunderstandsB	ultimatumBtrumanBthunderbirdsB	threatensBsupplyBstudiesBspockBsometimeBsomedayBslavesBskitsBsketchBsinkingBservingBrouteBropeB	realitiesBreaderBracesBpursueBpumbaaBpoundsBposeyBpeteBpeckBpatriciaBowesBoutlineBobtainBnominationsBnewmanBneuroticBmontyBmiceBmailBlegendsBleadersBlastsBlaputaB	justifiedBjuddBjokerBinstitutionBhungryBhighwayBhamBhackBgriefBgalaxyBfrankieBforgivenBflowersBfiringBfarmerBexteriorBeternityBentitledBduvallBdisgraceBdirtB
despicableB
derivativeBdemonicB
definitiveBdeclineBcrossingBcrispB	courtroomB
continuingB
chroniclesBcbsBcareyBbusterBbullyB
braveheartB	blatantlyBbittersweetBbatesBbasketBbarrelBassassinationBalleyBakinBagencyBaffectsB1977BwtfB
werewolvesBvocalBvanillaBunsureB
unlikeableBtowerB
terminatorBswallowBsubtlyBstrandedBsleazeBskitBsimpsonsBsfBservantsBserumBsendingBsalmanBroundedBronaldBreviewedBreliableBreeveBpuppetsBpunchesB
psychopathBpromptlyB
professionBprayBpixarBpeacefulBpcBnopeBneatlyBnailsBmythBmouthsBmoronsBmoralsBmercyBmccoyBmarionBmachoBlorettaBleapBlaborB
kidnappingBkidmanBjudgmentBjodieBjawBinspirationalBhippiesBhidesBhadleyBguineaBgrainyBgalBfrontierBfrancesBflashyB	fairbanksBexploitsBemergeBeliteBdragonsBdownsBdevastatingB	delightedBdarnBcreekBcrashingB	completedBcombinesBclashBchavezBcattleBcasinoB
cartoonishBbittenBauthenticityBapproachingB
accessibleB	acceptingB28B23B1981ByawnBwaltBuniformB	underwearBunattractiveB
unansweredBtopsBtonBthrilledBtenantBtabooBsteamB	startlingBspiceBspeechesBsidewalkBshootoutBseveredBsaleBroutinesBriversBrifleBrickyB
rebelliousB	publicityB
psychologyB	preparingBplayboyBpinBpennB	originalsBominousBnormBnolteBnetBmesmerizingBmasonB
mannerismsBloweBlongingB
lonelinessBliftBletdownBleighBlandedBkoreaBkentBjulesBjaredBjadedBitemB	isolationBintentionalB	incapableB
inaccurateBgungaBgoodingBgiantsBgeorgesBgableBfreezeBflashesBfactorsBexpertlyB
executivesBeventualBeatsBearnestBdudleyBdepthsBdemonstrateBdeliveranceBdeliciouslyBcushingBcontributedBcontinuallyBcontemptBconanBclimbBcharacteristicsBcarmenBbutcherBburkeBbroodingBbloomBbitchBarabBantonioBaimingBaidedBadoreB95B1974BBzaneByouthfulByoursBwheresBvanessaB
underwaterBtossedB
threatenedBtaxB	tarantinoBtaraBsydneyBsubtextBstringsBstorysBstickingB	spectacleBsixthBsharingBsensibleBseedyBscrewB	salvationB	repulsiveBreminderBregardsB	redundantBprovingB	preferredBpostwarBpiratesBpirateB
perceptionBpaulaB	parallelsBownedBolBnyBnutB	mythologyBmumBmotiveBmixingBminBmermaidBmasterfullyBluisBlongtimeBlimitationsBlaurenBlabelBkathyBjudgedBjerseyBjacketBireneBinsanityBimoB	housewifeB	horrifiedBhindiBheavyhandedBheavensBhandheldB	graveyardBgratefulBgoodsBgodawfulBgluedBfeastBexpectationBexorcistBexitB	estrangedBelectionBdjBdivineBdistressB
disastrousBcritiqueBcorbettB	convictedB
contributeBcontestantsB
containingB
comprehendBcommitsB
commitmentBclaraBchristieBchoppedBcarellBcapeBbutchB	breakfastBbostonBbombsBbmoviesBbeltBbarsBbacksBbachelorBbachB	awakeningB	attackingB
astoundingBassuredBasiaBandreBamritaBalisonB1991B1975B1950BxfilesB
witchcraftBwireB
winchesterBwardrobeBvileBvastlyBvariedBtraceBtoniB
timberlakeBthirtiesBtapedBtailBswearingBstrictB	strengthsBstoppingBstalkingBspiesB	sickeningBshorterBsgtBsergeantBscratchBscoredBromeoBrationalBquentinB	publishedBpredictablyBpolicyBpatternBpainterBownsB
noticeableBnieceBnbcBmoronBmoeBmiyazakiBmiloBmelvynB
meanderingBmarthaBmarilynBlunchBkurosawaBkeysBjediBjacquesBinsomniaBinherentBinformedBimpliedBimmortalBhugelyBhepburnBhatefulBgripBglobeBghettoBgainedBfreakyBfreakingBfragileBfordsBflimsyBfilthyBfifthBexposeBexpenseBeuropaBerrolBemailBebertBdrinksBdenyB	deliciousBdegreesBdameBcrosbyBcoreyBcloneBcherBbuzzBbuseyBbrunoBbronsonB	breakdownBbreadBavoidingB	attendingB
approachedBadaptionB1982BzorroBwilderBwhiningBvinceBvegaBtvsBtransformedBtonysBtobyBtheoriesBthelmaBsymbolBswedenB	suspectedBsurvivesB
suggestingBsugarBsublimeBsteeleBstatueBstatingBspitBslideBselectedB	screwballBsandBsafelyBrookieBrespectableBrescuedB	referringBrapidlyBrampageBquantumBpursuedBpuertoBpromotedBprogramsBproductsBpreachyB	populatedBpfeifferBpeculiarBpaleBomarBodysseyB	occasionsB
obligatoryBnetflixBmotionsBmossBmodestBminsBmathieuBlikeableBlexBlastingBknocksBkinnearBkarateBjanesBiranianBintroducingB	incorrectB	inclusionBhybridB	holocaustBhayworthBhackmanBgroundsBgoldblumBglaringBgilbertB	gentlemenBfeminineB	fastpacedB
farfetchedBexpectsB	europeansBerrorBenthusiasticB	enigmaticBdylanBdumpBdumberBdreadBdistractionB	decidedlyBcriesBcopiedBconsistB
confrontedBcoloursBcolinBcohenBcoatBcliffhangerBcleaningBcheaplyBcentreB
celebratedB
caricatureBbountyBbiblicalB
approachesBapplyBadmiredBaddictBacquiredBachievesB99B300B1939B1920sByepBwiderBupdateB	unusuallyBunimaginativeBuhBturdBtunedB	translateBtraitsBtorontoB	tormentedB	tolerableBthievesBsyndromeBstylizedBstationsBsophieBsnuffBsettledBserialsB	seductiveBseasonedBsamanthaBroundsBrewardBrepliesBrelentlesslyB	regularlyBrefersBreadilyBrainyBradicalBqueensB	principleBpoundBposingBposesBpleasingBottoB
optimisticB	obstaclesBnuancesBnovakB	nightclubBnarrowBmysticalBmessingBmamaBliuBlethalBlesterBlendsBlasBkazanBjulietteBjordanB	intricateBinteractBimmenseBhatsBhabitBgrassBfrogBfreddysBformedB	firstrateB	festivalsBestablishingB	establishB	energeticBdelightfullyBdebbieBdaylewisBcyborgBcounterB	convincesBconsiderationBconsequenceBconradBconfrontB
collectiveBcloudsBclassyBchongBcarnageBbyeBbudgetsBbillsBbikiniBaxeBaussieBauditionB	assembledBarrivingBansweredB	alexandreB	afterwardB200B1998B1000BwoundsB	wellactedBverdictBupsideBturmoilBtrainsBtireBtendencyBswitchedB	suspicionBstrokeBstewartsB
soderberghBsethB	sarcasticBsammoBsalesmanBsaintBrunnerB	relevanceBrealisticallyBramblingBprosBproceedBprecodeB
possessionBporterBpocketB
pedestrianBpayoffBpalmaBpadBoriginsB
noteworthyBnaschyBmoviemakingBmisunderstoodB
misfortuneBmirandaBmessyBmedievalBmeantimeBmealBmannersBlongestBliteralBlionelB	lightningB
legitimateBknightsBkellsBjennaBjacksonsBintactBinsistBinmatesBindianaB
improbableBhornyBhinesBheadacheBgusBgarlandBfollowupBflavorB
fassbinderB	explodingBexplodesBexperiencingBexcruciatinglyBevelynBescapingBenhanceBeinsteinB	dominatedBdivorcedBdismalBdeterminationBdeputyBdepictB	demandingBdefiesBdeerBcoveringBconveyedBcollectBcliveBclayB	cigaretteBbwB	brainlessBbellyBbeckhamBaustenBarrangedBarquetteB	armstrongBarguingBaptBagonyBadditionallyBaccountsB	absorbingB4thB21B1985B1940ByoutubeBwineBvolumeBvirtualBunsympatheticBtromaBtrapsBtoolBthrustBtacticsBswingingBstellaBstanceBstalkerBspineBspanBsondraBshepardBsentimentalityB	sebastianB	scriptingBscottsBscheduleBrevoltBreverseB
resemblingB
relentlessBrehashBrearBracingBpotterBplanesBpickupBpassageBparticipantsBpaintsB	overboardBoctoberB	obsessiveBobserveBnewcomerBmuteBmormonBmidstBmccarthyBmatesBmagicianBmacabreBlunaticBlukasBlowkeyBlongoriaBlolaBlighterBliamBlendBleatherBlaurieBkidnapBjuanBjewelBjealousyBinviteB	instancesBignoringB	identicalBhostageBhermanBgroundbreakingBgoalsBgeishaBfundingBfortiesB	followersB	fishburneBfataleB	explosiveBexcruciatingBexclusivelyBensureBenhancedB	enchantedBegyptianBeducatedBearnBeagerlyBdumpedBdumbestBdocumentBdistractBdilemmaBdeedBdahmerBdafoeB	conductorB
complimentBcomebackBcombsB
colleaguesBcoffinBclickBchillBcheekBcharacterizationsBcharacterisationBchamberlainBchamberB
cassavetesBbsgBboyerBbootsBbonnieBbiopicBbegsB	backstoryBartistryBarrowBarkinBarielBanxiousB	antonioniB	anthologyBalotB	aftermathBadrianB85B1959BwormsBwiselyBwhoreBwartimeBwagnerBvanityBunsatisfyingB	unnaturalBtremendouslyBtongueincheekBtargetsBsuckerBsubwayB	stephanieBsteerBstardomBstabbedBspiderBsmellBslipBslimBsholayBshiftsBshepherdBsg1B	separatedBseniorBseatsBschemingBrupertBromancesBrebelsBrealizationBpushesBpsychologistB
progressedBproducesBpoppingBploddingBplatformBpenelopeBpansB	overblownBoriginBoccultBnerveBnarratedBmurkyB	murderersBminnelliB
liveactionBlipBlinkedBjudeBislandsB	instinctsBindicateBhypedBhomosexualityBhoBheathBhavocB	furnitureBfrightBfoxxBforgivenessBforemostBfluidBfillsB	extensiveBenvyBembraceBedithBeconomicBdynamiteBdubiousBdreamyBdramaticallyBdiverseBdisappearanceB	dillingerBdiaryB
depictionsB
denouementBdeniseBdemonstratedBdefineB	deathtrapBdealerBdashingBdarthBdadsBcycleBcrippledBcreasyBcorporationBcornB	christineBchoirBchibaB
carpentersBcapacityBcannesB	breathingBbratBbranaghsBbonesBbleedBbeggingBbauerBattendedBarrestBappealsB
antagonistBallyB
aggressiveB	addictionBaboundB1992Bww2B	worldwideBwayansBwastesBwarnsBvargasBunBughBtrivialBtouristBtonesBthoBtcmBtargetedBsweatBsterlingBsparksB	spaghettiBsourBsketchesBsellsBseemingB	scoobydooB	scenariosBsatisfactionBrussB	rosemarysBriderBriceB	revolvingBresultedBrespectsB	repressedB	remindingB	releasingBreiserBrandallBrampantBpuzzleBpsycheBpredatorB	possessesBpornographyBplanetsBpickfordBpauseBpainsBpBoharaBobservationsBnivenBmustveB
motorcycleBmobileBmissileBmickBmadsenB	madeleineBlurkingBloyBlionsBlindyBkellysBjoseBiraBinvestigatorBinvestedBimpliesBhowlingB	homicidalBhardenedBgoodlookingBgeorgiaBgenerateBgangsBfrozenB
foundationB
forgettingBfoolishBfleshedBfleetB	exploitedBevansBentranceBenduringBencounteredBemployeeBelevatorBdrabBdominicBdiscoBdestinationB	deliriousBcrownBcrossesB	criticizeBcookieB
convenientBconsequentlyBconsB
compensateB
comparableBcolumbiaBclooneyB	christinaBcenaBcelebritiesBcarlitosB	blandingsBblamedBbikerBberkeleyBbenefitsBbachchanB
assignmentBarrayBappliedBampleBamidstB	ambiguityBadmittedB34B1944B19BzByellB
wonderlandBwipedBwhitesBwheelBvivianBvintageB	victorianBveteransBvalidBuniformsBunappealingBultraBtraumaB
traditionsBtoolsBticketsBtherapyBtalkyBsunriseBstartersBssBsourcesBsopranoBsitcomsBsinsB	sincerityBsightsBshotgunBshoreBsensibilityB	semblanceBseduceBscorseseBsandersBsamuelBruiningBrowlandsBrivalryBritchieBrewardedBreuniteB
resistanceB	remembersBraunchyBratsoBquestioningBquarterBpuppyBpoliticiansBplottingB
playwrightBphonesBparkingBparanoidBpapersBpacksB
outlandishBopenlyB	offscreenBnoveltyBnolanBmysteriouslyBmiraculouslyBmeteorBmensB	mastersonBmartianBlaunchB
laboratoryBkubricksBkirstenBjuryBjulyBjolieBjjBjigsawBjessBiturbiB	inventionBingridB	incidentsBhuntedB
hitchcocksBgloriaBglimpsesBgilliamBgiganticB	gatheringBgarfieldBfriedBfranticB
favouritesBdividedB	diversityB
distractedBdevotionBdeskBdenisBdecidingBdeannaBcurlyB	curiouslyBcubanBcoworkerBcowBcountingBcormanBcookingBconventBcolletteBclichedBcirclesBchucklesBchewBchesBcheersB	centuriesBcecilBcapBbridesBbreastBbossesBblaiseBbertBbabesBartyB
apocalypseBanytimeBallegedBajayBacknowledgeBabortionBabandonB1953BwrightBwongB
witnessingBwidowedBwakingBvomitB
villainousBupcomingBunhingedB	unfoldingBumBtrierBtomeiBtombBtimmyBsurroundingsBsundanceBsubsequentlyBstereotypedBstardustBstalkedBspoilingBspencerBsonnyBsmashBshrekB
shockinglyBsheetsBsheenBshaggyBserbianB	sentencesBsearchedBscreenedBsarandonBsaltBsailorB
sacrificesBruledBroughlyBrivalsBripsB	rewardingBrepublicBrememberingBrelatingBrejectsBreaBranchBqualifyBqBpythonB
protectionB	premingerBpredecessorsBpointingBpivotalBphiloBphaseBphantasmBperilBparksBorphanBordealB	orchestraBoliviaBoldsBoldfashionedBobservationBnuancedBneedingB
moviegoersBmockBmiikesBmcqueenBmaximumBmanicBlucilleBlopezBlavishBlastlyBlabeledBkinskiBjanetBinterruptedBindifferentBhystericallyBhootBharronBguardianBgretchenBgalleryBgadgetsBfuelBfoilBflowerBflockBflamesBfilmographyBfeebleBfanaticBfadesBensueBelliottBeliBdynamicsBduhBduelBduBdougBdooBdismissBdespiseBdemilleB	defendingBdeemedBdebtBcypherBcrushedBcrossedBcrashesBcountyB
conventionBconvenientlyBcontroversyBconfuseBconfinedBconclusionsBconcentrateBcommendableB	colleagueBclerkBcheadleBcharliesBcaronBcampusBcampingBbrashearBboutBbennettBbegBbcBbatsBbakshisBbackedBavoidsBavidBassureBanticipatedBachievementsB
accidentalB1948B101B
youngstersBwinnersBwhinyBwhereverBwendigoBweeklyBwarholsBvoyagerBvoidB	vignettesBverbalBupbeatB	unleashedBuncannyBtheatresB	surrenderBsupremeBsummedB
sufficientB
statementsBstadiumB
similarityB	silvermanBshapedBservicesBselfindulgentB	schneiderB
scarecrowsBsanityBrosesB	rodriguezB	robertsonBresumeBreservedBreplacementBrepeatsB	reflectedBreeseBragingBpreciseBposeBpollyB
phenomenalB	pervertedBpazBpattyBpathosBparticipateBoverbearingBorganizationBoffendB	obscurityBnorthamBnetworksB
nauseatingBmovieiBmotelBmortalBmonksBmomsBmeyerBmentorBmelodyB
melancholyBmedicineBmaskedB	marijuanaB	maintainsBlicenseBlayingBkeatonsBkatieBjudgesBjeBjarB	intenselyBinsertBinjuryB
incidentalB
incestuousBinadvertentlyB	imaginaryBidolBhustonBhostelBhopBhmmBhayesB	harrowingBhandicappedBgutBgrabbedBgielgudBgacktBfruitBfraudBfratBfiresBfiendBfactualBexistingBexcusesB
enchantingBenBembarrassinglyBelsaBeffortlesslyBealingBdustinBdivisionBdistrictB
displayingB
deliberateBdaisyBcutterB
cunninghamB	considersBcompositionBcommunicationBcomaBcolmanBcollinsBclosureBclaudeBchloeB	childlikeBcheeringBcarpetBcapoteBbuttonsBbugBbrookeBblockbustersBbiasBbernsenBbattlingBbarbraBbadnessBbaddiesBappleBangelinaBangB	ambitionsBaffleckB	admissionB1967B010BwrestlerBwarrantB	viewpointBvibeBuninspiringBuneasyBunderstandablyBunderdevelopedBturkishBtransparentBtoxicBthurmanBthunderBthailandBtakerBsuckingB
substituteBsubstantialBstumbleBstreamBstilesBsoylentBsolvedBslappedBskinnyB	skepticalBsheepBshahidBseymourBsergioBsarcasmBroboticB	reviewingBreportedBredneckBrecreateBrecklessB	rebellionBreBpiaBphoenixBpathsBparrotBpalmBoutrageouslyBoprahBmarcelBmanipulatedBmaldenBmaintainingB	magazinesB	macmurrayBlensBleanBkruegerBjokingB	johanssonBjoBjacksBintroBinteriorBinaccuraciesB	immigrantBignoresBgwynethBgratingBgrandmaBgracesBgorillaBglanceBgigBfuzzyBfundamentalBfrontalBfranklinBflowsBflagBfinishesBfarmersB	fairytaleBfacilityBexplodeBexcelsBeverettBepitomeB
enormouslyB	employeesBechoesBearliestBdwightBdriversBdrawingsBdraggingBdominateBdirecttovideoBdamselBdallasBcunningBcradleBcomplicationsB
committingBcoloredBcoburnBclunkyB
classmatesBclarityBcindyBchopsBcheechB
captivatedBcanceledB
boundariesBbonBbogartB	bodyguardB	bloodbathBblamesBbitingBbenjaminB
beckinsaleBassetB	arroganceBambianceBamazonBalaBadmitsBaccompanyingBabuByellsBwoefullyBwaynesBwardenBvividlyBvincenzoBvergeBunstableBunravelBtitularBtitsBtideB	throwawayBtemperBtakashiBsystemsBsuzanneBsustainBsubgenreBstudiedBstakeB	spidermanB	spaceshipBslyBslowerBslashersBshredBshoeBshocksBshiftBshannonBshahBsessionBsensitivityBsecureBscriptwriterBscarlettBsatanicBsaloonBsailorsBrussiansBrourkeBrollsBrewriteB	renderingBrejectBrealisesBratioB
protectiveB
principlesB	preachingBpoeBplateBpizzaBperiodsBpennedBpeckerBpalsBobrienBnutshellBmyrnaB
monotonousB
mediocrityBmeadowsBlynchsBlistenedB	lingeringBkareenaBjustificationBjaggerBinterviewedBinterpretationsBinformerBinformativeB
idealisticBhumBhawnB
happeningsBhallmarkBhainesBgrooveBgreaseBgoldieBglowBglossyBgesturesBgarageBfanningB
excellenceB	espionageBernestBepicsBentriesBengineBedisonBdrowningBdistributedBdistinctionB
disciplineBdisappointsBdisabledBdimBdiegoBdeadpanBcryptB
criticizedBcrashedBcontrollingB
contestantBconstraintsB	consciousBconnieB
commandingB	combiningB	collectorBcokeBcmonBcloakBclimbingBcheerfulBcandleB	cancelledBcainBburntBbuffyBbuddingBbubbleBborrowsBboardsBblessBblackandwhiteBbegunBbeersBbeardBbalancedB	automaticBatrocityB	associateB	argumentsB	annoyanceBanchorsBaircraftB	affectingBadaptBabominationBaaronBzodiacBwesleyBwatcherBwashedB	warehouseB	vigilanteBvetBversaBvalB
unfamiliarBunclearB	traumaticB
transformsBtormentBtonedBtightlyBtherebyB
thereafterBtestingBsunkBstrungBstraighttovideoB
stepmotherB	spotlightBspontaneousB
somethingsBsociallyBsnipesBsmugBslaveryBsinksBshawnB	sasquatchBsangBsaneBsalvageBromaniaBrocketsBrobbersBrobbedBrightlyBreunitedB
retirementBresolveBremarkBrelaxedBramBraidBquintessentialBprotestBpresidentialB
prejudicesB	practicalBpoppedBpilotsBpigsBphillipBperspectivesB	permanentBpassionsBnoticesBninjasBnightmarishB	neglectedB	necessityBmooresB	monologueBmaureenBmarineBmanipulationBlockeBlinearBlilBlibertyBlandmarkBkrisBkindlyBjuiceBjanB
investmentB	interplayBintendBintellectuallyBinformsB
imprisonedBhannahB	griffithsBgloomyBgiovannaBghostlyBghastlyBgentlyBgapsBgapBfritzBforgetsB	finishingBfascistBfairnessBexploitativeBevokesB	elephantsBehBedmundBdunneBdrownedBdreyfussBdivingB	distortedBdistinctiveBdinerBdiazBdebraBdaytimeBdavidsBcortezBcollaborationBclinicBchipBchaoticBcedricBcasperB
casablancaBcarlosBcapricaB	cameramanB	butcheredBbrentBboobsBblondellBbeanB
battlestarBbarnesBbanksBaztecB	awarenessBavengeB
astronautsBappliesBapolloBannoyB	animatorsBangieBaltmansBalmightyB	alexandraB	aestheticBadmirerB	addressedB1963B1943BwhodBwearyBvisitorBvcrB	variationBursulaBunconventionalBtruthsBtinaBtendedB
temptationBtechBtashanB	switchingBsupermarketB	submarineBsteamyBsteamingBstabBspiralBsophiaBsnowmanBsimplerBsicknessB	seductionBscarierBsandyBsaintsBromanianBrodneyBrevivalBrenoBreluctantlyB	reasoningBrapidB	radiationBpursuingBpsychedelicBprologueBprogressionB	programmeBprofessionalsBpretendsBpotentBpornographicBplotlineB	placementBpierreBperverseBpairingB	overtonesB
ostensiblyBoscarwinningB
oppositionBongoingBnorrisBniftyBnataliB	motivatedBmondayB	miyazakisBmistyBmisterBmidgetB
mercifullyBmarvinBmarisaBmangaBloonyB	lookalikeBliottaBlifelongBlegionBkristoffersonBjoiningBisabelleBinsertedBinexperiencedBinclinedBincestBimhoBillusionBhooperBhookerBhometownBhiphopBhelmetBhazzardBhahaBgraduateBgovindaB	gladiatorBgeneticBfreedBforrestBfileBfightersBfetchedBfederalBfashionsBfartBfamedBexplanationsBewoksBevokeBerBegyptBdukakisBdriftBdownbeatB	disregardB	discussedBdernBdemeanorBdeedsBdecencyBdashBdarklyBdangerouslyBczechBcursedBcrooksBcringingBcravensBcracksBcontrolsB	confrontsBclutterBclientBcladBcircaBcharacteristicB
censorshipBcassieB	caretakerBbustBblessedBbelleBbashingBatlanticB
assistanceBarmorBarcherBapplauseB	announcedBallisonBahmadBaccomplishmentB26B1966B1951BzuBzizekBzanyB	wholesomeB	whimsicalBwarnersBwalmartBvoodooBunnecessarilyB	underusedBunderstatementBtroopersBtravisB	transportB
transplantBtautBtaimeBsurroundBsurfBstrainedBsternBspottedBspoofsBspellingBspelledBsophisticationBsnapBsmithsB
slowmotionBslippedBsleptBslaterBshieldBshelvesBsharksBshaolinB	shamelessBshamefulBseussBsensationalB	sensationBseBscreenplaysBsammyBsalesBrousingBrkoBritualBridersB	retellingBrelyingB	qualifiesBquaintBproudlyBprostitutionBpropB	projectedBpriestsBpresumeBprefersBpompousBpolishBplugBplacingB
photographBpenBpaycheckB
paperhouseBpairedBovershadowedBoutsetBoutlawB
officiallyBoccupiedB	obliviousBnoamBnearestBnatashaBmyraBmurielBmostelBmonroeBmomentumBmobsterBmishmashBminesB	melbourneBmarkedBmarinesBmaguireBluzhinBltBlonerBlocateBliftsBlevyBlesbiansBkneesBkidnapsBkennyBkeanuBkathleenBkB	judgementBintelligentlyBinformB
infinitelyBincreaseB	imitatingBilB
identitiesBhugoB
highschoolBherdBherbertBhenchmanBheavenlyBheatherBhauntBharilalBhansBgruffBgrosslyBgrislyBgolfBglendaBgiBgeekBgatheredBgameraB	functionsBflashingBfieryBfierceBfiancéeBfetishBfenceBfailuresBernieBelevateB
electronicBelectedBeagleBdunstBdrummerBdreamingBdoyleBdodgyBdetroitBdealersB	customersB	coworkersBcowardlyBcourtesyB
courageousBcounterpartsBcorkyBcoopBconvictB	conveyingBconnorB	connivingB	condemnedB
complainedBcolonyBchopB
cheesinessBchargesBcautionBcaperBcabBbusbyBbreakthroughB
boyfriendsBborrowBblazingBbitesBberengerBbatwomanBbattlefieldB	bartenderB	awfulnessBaweighBawaitingBaugustB	architectBantiheroBanitaBanalyzeBalarmBadvisedBadvancesB	abundanceB51B20sB1958B1955B1946B1934B1932BzealandByBwrathBwolvesBwitsBwhipB	weirdnessBwashBvertigoBvaughnBurgencyBupstairsB
upbringingB
unemployedB
undercoverBtrickedBtraveledBtolerateBthroneBtepidBtastyBtackleBsymbolsBsweepingBsurgeonBsurfersB
supportiveB	superstarBsunsetB	stumblingBstuffedB
standpointBstagingBsleuthBsiblingB	showcasesB	shootoutsBshoBsemataryBseattleBsealBsatisfactoryBsaraBromerosBroadsBrevelationsB	restraintBrespondsBrespondBrepresentingB
reportedlyB	remainderB	rehearsalBregimeBredgraveBrapesB
protectingBproportionsB	promotingBprankBpondB	policemenBpolarBpokesBplayfulBpeggBoverwroughtBoperateB	officialsBoffenseBnunsBnotwithstandingBmorseBmoleBmindsetBmiddleclassBmechanicBmartiansBmarryingBmarcusBmackBluxuryBlureBloganBloBlindseyBlaunchedBlaserBlarsBlaraBlamasBknockingBjointBirvingB	irritatedBirresistibleB	intellectB	injusticeBinfluentialBimplyBiconsBhurryBhunkyB	hungarianBhostileBhintedBhartleysBharborBhairyBgrinBgreeneBgravityBgrandparentsBgovernmentsBginoBgiggleBgeoffreyB	galacticaBfurBfrostB	fortunateB	footstepsBfodderB
flamboyantBfinneyBferrellBfelliniBfabricBexpertsBexpandBenglundBengineerB
encouragedB	emphasizeB	dreamlikeBdrainBdishBdependBdenyingBdeckBdaylightBdataBdaniBcustomsBcrowdedBcrookedBcrocBcoverageBcountrysBcountedBcoolestBconsumedBconsciousnessB
confessionBcockneyBcocaineBclimbsBchokeBchevyBceilingB	cannibalsBcalBbyronBbtkBbrushBbritneyBbreatheBbrandonBboostBblissBbitchyBbelievabilityB
beforehandBbearableBbartonBballroomBassumesBarticleB
apprenticeBantiBanguishBamidBalliesBallanBaliB
alcoholismB
admirationBacclaimBabroadB48BzelahByarnBwronglyBwillemBwieldingB
wheelchairBvetsBveraBvaryingB	unnervingBtrustedBtriggerBtrampBtorchBthugB	terrorismBtediumBtasksBsylviaBsupportsBsuperfluousB
successionBsuaveBstrainBstirringBstereotypingBstefanBstarshipBstalksBspadeBsoberBsnatchBsleepyBslaughteredBsixteenBsitesBsheridanBshadyBshadowyBsessionsBsensualBsenatorBselfcenteredBsectionsB
scratchingBrunawayBruddBrogueBrobbingBrichlyBrepresentativeBrelatesBrebeccaBrazorBraptureBpubBprostitutesBprophecyBprolificB	practicesBpitaBperceiveBpenaltyBpeersB	patrioticBparodiesBoweBoutsBoutbreakBobservedBnuttyBnovelistBnotionsBnormaBneverendingBnailedBmuslimsBmudBmjBmixesBmissionsBmischievousBminingBmauriceB
mastermindBmarquisBmandyB	mandatoryBmaidenB	magicallyBluridBlungBlucioBlorreBlombardBlistingBlightweightBldsBlatinoB	languagesBkamalBjurassicBjohnsBjewelryB
intestinesBinhabitBinconsistenciesBimpressionsB	impendingB
impeccableBillustratedBidiocyBhoganB
hobgoblinsBheroinesBhenchmenB	heartlessBhealingB
hardboiledBhamillBgustoBgrendelBgoodmanBgoersBgodardBgiftsBgibsonBgershwinB
gandolfiniB
fulfillingBfrenzyBframingBflowingBflameBfearedBfayeBeyedBexposingBexamineBenthrallingB
entertainsBedgesBeconomyBebayBearnsBdorisBdopeyBdisdainBdiscernibleBdiggingBdevelopmentsB	determineBdetachedBderivedB	depardieuBdefyB
dedicationBcowroteBcowardBcorrectnessBcoppolaBcontributesBcontinuouslyB	continentBconfinesBcompoundBclothBclimateBclawB	civiliansBcinematographicBchewingBcheungBcensorsBcellsB
categoriesBcaptiveB
capitalizeBburialBburdenBbrennanBbothersBbondsBbleedingBbingBbenoitBbehavingBbehalfBbearingBbarneyBawryBawardedBautoBauraB	attendantBarnieBallianceBalienateBadsBabsorbedB700B55B510B400B1957BwolfmanBwokeBwidowerBwhippedBwheelsBweaverBwaltersBvisceralB	virginityB	violentlyB	villagersB	victoriasB
verhoevensBunconsciousB	uncertainBtuckerBtruthfulBtreatingBtransitionsBtouristsBthumbBthroatsBthreadsBthinnerBthereofBthatllBteddyBtablesB	superiorsBsubmitB
structuredBstrippedBstatureBstabsBspringsBspittingB
spielbergsBsnowyBslipsBslamBskippingBsinatrasBsimonsBsimbaB	signatureBshelterBshaunBsexistB	sentencedBsemiBsciencefictionBsaifBsaddestBrustyBrussoBruggedBrollerBroachBrhymeBregisterBrantBpuzzledBpsychiatricB	prolongedBprogrammingBpremisesB	premieredBpredictabilityBpranksBpouringB
portugueseBplatoonBplantsBplaguedBpeggyBpayneB	passengerBpaddedBowlBovertBoveractsB	opponentsBoperasBnunBnoraBnestBmythicalBmustacheBmoldB	misplacedBmiraBmillsBmeredithBmeltBmelindaBmeaningsBmatchingBmartinoBmaeB	macdonaldBmacbethBmaBlubitschBlizardB	libertiesBledgerBlaysBlawyersBlargestBkaufmanB
kaliforniaBjedBjamBjackmanBinterspersedB	interiorsBimitateB
illustrateBidealsBicyBhypnoticBhugBhopefulBhoneyB	hendersonBheirBhatingBharlemBhandyB
hammerheadBhallucinationsBhalfhourBgimmicksBgeniusesBgenaBgenBgeeBfussBfunkyBfloorsBflipBflewBfastforwardBfadedB
expressingBerikaBentertainerB
engagementB
encouragesB	embarrassB
eliminatedBelderBeditsBechoB
dreadfullyBdrasticallyBdownfallBdominickBdomBdiverBdillonB
diabolicalBdeepestBdcBdarlingBcurtainBcrowdsB
criticismsB	cowrittenB	convertedB
connectingB
conceptionBconcentratesB
companionsBcollapseBcohesiveBcockyBclydeBchimneyBchenBchairmanB	celebrateBcathyBcastroBcarlitoBcapsuleB
capitalismB
candidatesBbsB	brazilianB	botheringBberserkBbehavesBbartBbaldBbaitBauteurBatticB
atrocitiesBastonishinglyBashB	artemisiaB	apologizeBanxietyB
animationsBamuseB	allegedlyB	achievingBacademicBaboardB2009B1965B1942ByearningBworryingBwondrousBwinstonBwhoveBvulnerabilityBvotingBviggoBviennaB
variationsBvapidBuntrueB
undertakerBunbornBturtleBtraumatizedBtossBtoppedBtilBtheirsBtextureBtangoBsykesBswitzerlandBswitchesBswatB	surpassesB	supremacyB	subtitledB	stupidestB
strikinglyB	stretchesBstreakB	strangestBstonedBstaresBspellsBsoreBslugBsloaneBshrinkBshrillBsherlockBseriousnessBsensibilitiesBsealedBscreamedB	scatteredBsackBrosarioBretrieveBrestlessBreincarnationBregretsBrefugeB
recoveringBrecoverBpurseB	punchlineBprophetB
profoundlyBprofitBprofileBprimalBpricesBpressedB	predictedBppvBportmanBpoppinsBpistolB	perceivedBpenguinBpausesBpatchB	paragraphBpaddingBowningBoverwhelmedBoverseasBouttaBoblivionBnovemberBnotingBninetyBnerdsBnapoleonBmutantsBmpaaB	monasteryB	moderndayBmmB
millenniumB
metropolisBmerryBmasseyBmarredB
manipulateBmanagingBloopBlocalesBlocaleBleopoldBlansburyBknackBkibbutzBjoviBjerkyBjanitorBit´sBisraeliBinsistedB	inhabitedBicebergBhollandBhispanicBhiltonBharmonyBharlowBgroundedBgrievingBgretaBgrandpaBgoshBgeeksBgardensBgageBfulcisBfugitiveBfryeBfreelyBflorianeBfleeingBfixedB
fitzgeraldBfiascoBfewerB	fathersonBfarrahBfarewellBfallonBexudesBextraordinarilyBevolvedBequalsBenlightenedBelmerBellaBecstasyB	eastwoodsBdwarfBdutiesBduryeaBduffBdrumBdreamedBdisappearingB
deservedlyBdelveBdeliaBdefiningBdeathstalkerBdaresBdangersBdaisiesBcrennaB	creationsB	confirmedBcondescendingBconcentrationB	competingBcompassionateBcolemanBcoalB	classroomBchaptersBchandlerBcatastropheBcarolineBcaptBcapshawBcanonBcampersBcallahanBburstsBburgessBbulliesBbrickBbrettBbranchBbondingBbombingB	blackmailBbetraysBbetrayedBbethBbelieverBbarmanB	backdropsBauntsB
attributesB	astronautBartisticallyBarizonaB
aristocratBarchitectureB
antichristBanistonBamoralBalvinB	admirablyBacquireB98B65B250B1931B
yourselvesBwwiBwuBwizardsB	willinglyBwbBwarpedBwaitsBvotersBvivahBvirtuesBvicB
vaudevilleB	vanishingBvanishesBuptightB	unwillingBunnamedB
uncreditedBtsuiBtrickyBtransferredBtomatoBtimelineB	tigerlandBthereinBthankfulBtessBtentB	temporaryBtellyBtanksBswordsBswissB	suspendedBsubduedB	stylisticBstrayBstinkBstillsBstealthB	spreadingBsportingBspectrumBspadesBsoullessBsorrowB	societiesBsnippetsBsmittenBslewBsleepwalkersBsignificantlyBshueBshowtimeBshoutsBshoutBshinyBshinaeBsheetaBshakesBshackBscriptwritersBschemesBscandalB
sacrificedB
sabretoothBrollercoasterBriddenBrevolverB	revoltingBretainBreplayBredfordBrecipeB	ravishingBrackBpuriBpsychologicallyB
proverbialB
pronouncedB	prevalentBpreteenB
preferablyBpourBpostmanB	positionsBposhBpokeB	pleasuresBpetersonBpertweeBpeaksBpaulsBpattonBpatternsBpalmasBoverusedBoutrageBorlandoBorgyBoptimismBonenoteBomBolympiaBolsenBnerdyBneoBneglectBmutedBmuscularBmurphysBmuniB
monologuesBmobstersB
mismatchedBmidwayBmcadamsB
marginallyBmabelBlordsBlordiBlimbsB
lieutenantBleapsBlawnBlanzaBlampoonBlambsB	kornbluthBkordaBkiddieBkeitelBjaffarBitiBirresponsibleB
irrationalBintruderBindulgeBindifferenceB
immigrantsBimmersedBhurtingBhoustonBhornBhockeyBhickockBhesitateBhesheBheroismBhereinBheapBguruBgripsBgradyBgomezBgoldsworthyBgleeBgaspBfreakedB	frameworkBforumB
forebodingBflaviaBflamencoBfauxBfanaticsBfableB	expressesBexpandedBexgirlfriendBexaminedBestablishmentBemmyBelishaBelectricityBeggB	efficientBdrumsBdodgeB
distinctlyBdiscussionsBdictatorBdestructiveBdenverBdenialBdellaBdeclareBdeborahBdaneBdakotaBcrawlBcrassBcowriterBcorbinB
continuousB	consistedB	concludesB
compromiseB	communismB	comicbookBcodyBclubsBclarenceBcheatBcheaperBceremonyBcbcBcavesBcarusoBcarelessBbrunetteBbritsB
breathlessB
borderlineBboothBbookerBbogdanovichBbeefBbastardBbashBbarkerB	backwoodsBathleticBateB
assortmentBassistB	arbitraryBappalledB	apartheidBannoysB
annoyinglyB	announcesBalternatelyBalliedBairingBaimsBadministrationB	addressesBacknowledgedBabstractB18thB150BwovenBworshipBwisecrackingBwelcomedBweavesB
voiceoversBvirginsB	versatileBvengefulBusageBunluckyBtroyBtrendyBtremorsB	transformB	toleranceBtobeBtestedBtensionsBtenantsBtackedB	surpassedB
supportersBstubbornBstoicBstimulatingBstarvingBstabbingBsqueezeBsnappyBsmarterBsleeperB	skywalkerBsilkBsiegeBshockerB	shatteredBshakespeareanBseasideBsearchesBscrewingBsammiBrumorsBrottingB	roommatesBromanoBripoffsBrewatchB
retrospectB	replacingB	repellentB	rejectionB
registeredBregalBredeemedB	recurringB	recreatedB
recognisedBrecallsBpuzzlingBproneB	promotionB	preventedB	pressuresB	preservedBpowsBportionsBpoliteBpoetBparticipatingB	overnightBorientedBopusB	operatingBolympicBogreBoddballBobsceneBnellB	mortensenBmiraclesBminionsBmeyersB	mcdermottBmaturityBmarxB
marvellousBmanufacturedBmadmanBlumpBlotrBlogoBlizBliliBliarBlanaBlampoonsBknoxBkeyboardBkerryBkeeperBjoannaBitaliansBirisBinterminableBinsultedBinnuendoB	inheritedB
influencesB
infidelityB
infectiousB
increasingBincompetenceBinchB
improvisedBimdbsBilkBhustlerBhorizonBhooverBhoodsB
hitchhikerB
historiansBhisherB	hindsightBhilliardBhilaryBherringsBhelenaBheiressBharrysBhanzoBhanBhaltBhaleBgypsyBgrumpyBgainingBfriendshipsBfortBforsytheBformalBforbesBfloatBflemingBfleetingB	firsttimeBfernandoBfeaturelengthB
farnsworthBfaintBextremesBexhibitBensuingBemploysBedwardsBdwBdustyBduchovnyBdruggedBdrippingBdrainedBdownsideBdogmaB
documentedBdistinguishedB	disastersBdigsBdiamondsBdeviousBdevganBdeniedBdemographicBdefenceBdarkestBdaftBcurrieBcrackerB	corridorsBcontemplateBconquestBconnollyB
conflictedBconcentratedB	comprisedBcompositionsBcommentariesBclumsilyB
classifiedBcircuitBchristensenBchoreBchockBchemicalBcensoredBcelebratingBcasuallyBcarnivalB
calculatedBcafeBbuildupBbryanBbrandosB	braindeadBbombedBbimboBbillingB	bickeringBbaseketballBballoonBbaffledBbaddieBbackyardBazumiBautobiographyBarchiveBapprovalB	andersonsBamokBalterBaielloB
affectionsBadventurousB
adequatelyB
addressingB	actualityB	accompanyB86B27B1949BzoomBzeniaBwryBwrestlemaniaBwitlessBwinsletBwillyBwhackedB
wellingtonBwelchBwaqtBvisitorsBviscontiBvinnieBvacuousB	upsettingBunwittinglyB
untalentedBuniversallyBunforgivableBtripsBtowBtomsB	tomlinsonBtintinBthornB	therapistBtheodoreBtextbookB
tendernessB
tearjerkerB
talentlessBswampB	summarizeBstupidlyB
stretchingBstoweB
stepfatherBstableBspectacularlyBspearsB	smalltownBsmackBslowsBshtickBshopsB	shootingsBshearerBseventhBselfconsciousBschtickBscaringBsacredBrunofthemillBrubBrockerBrigidB
rightfullyBricoBresurrectionBrestoreBrequestBrenderBrelaxingB
recreationBrecommendingB
reanimatorBranmaBrangingBquirksBpursuesBpunsBpulseB	prototypeBpremierB	precisionB
powerfullyBposedB	poignancyBplantedBpicnicBpetsB
perversionBpercentBpbsBpartialBparsifalB
overweightBovertlyBoveractBouttakesB
ossessioneB
orchestralBoptionsBoneillB	offeringsB	occurringB	norwegianB	northwestBninetiesBneesonBnasaBmythsBmotifB
moonstruckB
monumentalBmontagesB	monstrousB
moderatelyBmockeryBmichelBmeekBmcgavinBmcBmarathonBmanosB
managementBluthorBlorenzoBlistsBlineupBlimpBlightlyBlentBlennonB
leadershipBlayeredBlangeB	labyrinthBkilmerBkeelerBjuicyBjoyousBjoshuaBjockBjewBjerksBjanuaryBjamesonBitbutBirsBinventBinterpretedB	interpretBintendsBintegralBinsignificantBinducingBillustratesB
illiterateBideologyB
identifiedBhousingB	honorableBhmmmB	historianB
hesitationBheritageBhectorB	hardshipsBhardestBhappierB	hamiltonsBhagenBhabitsBgramsBgossipBgooBgamutBfulfillBfoxesBfostersBflippingBfleaBfinnishBfiguringB	feinstoneB
featuretteB
expressiveBevolveBethicsBeppsB	eponymousBepisodicBenhancesB	empathizeBemergedBelliotBeleganceBeggsBdrippedBdownwardB	documentsBdistributorsB
disserviceBdirectorwriterBdigressBdifferBdesolateBdenseBdenchBdemonstrationBdecemberBdancedBcynicismBcuesBcroweBcropBcrookBcousinsB	costumingBcopingBconvictsB
consistingBconroyBconnectsBconductB	complainsBcommandoBcoenBcloudBclosesBclimaticBcleanerBchatBchamberlainsBcastedBcanvasBcagesBburyBbureauBbucketB	broderickBbrockBbritBbrinkBboyishBboxesB	bloodshedBblokeBblindedBblendingBblaineBbinocheBbillionBbigfootBbetsBbensBbennyBbeetleBbathtubBbathingB	barbarianBbalconyB
babysitterBatwillB
attributedB
astonishedBassociationB
associatesBashrafBaptlyBapproximatelyB	apologiesBaplombB	answeringB
alienationBalbaBactorsactressesBabbottB64B500B43B1938BzooBzhangByeaBwyomingBwrayBwormBwoefulBwitherspoonBweiszBweirdoBweeBwavingBwarningsBvirtueBvh1BvaultB
uneducatedBundevelopedB
unbearablyBturgidB
travellingBtransvestiteBtransportedB
tragicallyBtownspeopleBtoesB	todeskingBsybilB
sweetheartB
suspicionsB	surroundsBsuperpowersBsuiteB
subtletiesBsubconsciousB	struggledBstripsB
streisandsBstirBstapleBstalwartBspotonBsparseBsorvinoB
sophomoricBsooooBsmoothlyBsmarmyBslutBskimpyBskeletonBsinginBshudderBshovedB
shoestringBsherryBsheilaB	shawshankB
separationBseinfeldBsegalBscenicBsarneBsabuBrushesBruntimeBrisksBreviveBreversedBretroBrestsBrerunB	requisiteB
repertoireBrenownedBrendersB	recogniseBreckon
��
Const_5Const*
_output_shapes	
:�N*
dtype0	*��
value��B��	�N"��                                                 	       
                                                                                                                                                                  !       "       #       $       %       &       '       (       )       *       +       ,       -       .       /       0       1       2       3       4       5       6       7       8       9       :       ;       <       =       >       ?       @       A       B       C       D       E       F       G       H       I       J       K       L       M       N       O       P       Q       R       S       T       U       V       W       X       Y       Z       [       \       ]       ^       _       `       a       b       c       d       e       f       g       h       i       j       k       l       m       n       o       p       q       r       s       t       u       v       w       x       y       z       {       |       }       ~              �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �                                                              	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �       	      	      	      	      	      	      	      	      	      		      
	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	       	      !	      "	      #	      $	      %	      &	      '	      (	      )	      *	      +	      ,	      -	      .	      /	      0	      1	      2	      3	      4	      5	      6	      7	      8	      9	      :	      ;	      <	      =	      >	      ?	      @	      A	      B	      C	      D	      E	      F	      G	      H	      I	      J	      K	      L	      M	      N	      O	      P	      Q	      R	      S	      T	      U	      V	      W	      X	      Y	      Z	      [	      \	      ]	      ^	      _	      `	      a	      b	      c	      d	      e	      f	      g	      h	      i	      j	      k	      l	      m	      n	      o	      p	      q	      r	      s	      t	      u	      v	      w	      x	      y	      z	      {	      |	      }	      ~	      	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	       
      
      
      
      
      
      
      
      
      	
      

      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
       
      !
      "
      #
      $
      %
      &
      '
      (
      )
      *
      +
      ,
      -
      .
      /
      0
      1
      2
      3
      4
      5
      6
      7
      8
      9
      :
      ;
      <
      =
      >
      ?
      @
      A
      B
      C
      D
      E
      F
      G
      H
      I
      J
      K
      L
      M
      N
      O
      P
      Q
      R
      S
      T
      U
      V
      W
      X
      Y
      Z
      [
      \
      ]
      ^
      _
      `
      a
      b
      c
      d
      e
      f
      g
      h
      i
      j
      k
      l
      m
      n
      o
      p
      q
      r
      s
      t
      u
      v
      w
      x
      y
      z
      {
      |
      }
      ~
      
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                                      	       
                                                                                                                                                                  !       "       #       $       %       &       '       (       )       *       +       ,       -       .       /       0       1       2       3       4       5       6       7       8       9       :       ;       <       =       >       ?       @       A       B       C       D       E       F       G       H       I       J       K       L       M       N       O       P       Q       R       S       T       U       V       W       X       Y       Z       [       \       ]       ^       _       `       a       b       c       d       e       f       g       h       i       j       k       l       m       n       o       p       q       r       s       t       u       v       w       x       y       z       {       |       }       ~              �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �        !      !      !      !      !      !      !      !      !      	!      
!      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !       !      !!      "!      #!      $!      %!      &!      '!      (!      )!      *!      +!      ,!      -!      .!      /!      0!      1!      2!      3!      4!      5!      6!      7!      8!      9!      :!      ;!      <!      =!      >!      ?!      @!      A!      B!      C!      D!      E!      F!      G!      H!      I!      J!      K!      L!      M!      N!      O!      P!      Q!      R!      S!      T!      U!      V!      W!      X!      Y!      Z!      [!      \!      ]!      ^!      _!      `!      a!      b!      c!      d!      e!      f!      g!      h!      i!      j!      k!      l!      m!      n!      o!      p!      q!      r!      s!      t!      u!      v!      w!      x!      y!      z!      {!      |!      }!      ~!      !      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!       "      "      "      "      "      "      "      "      "      	"      
"      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "       "      !"      ""      #"      $"      %"      &"      '"      ("      )"      *"      +"      ,"      -"      ."      /"      0"      1"      2"      3"      4"      5"      6"      7"      8"      9"      :"      ;"      <"      ="      >"      ?"      @"      A"      B"      C"      D"      E"      F"      G"      H"      I"      J"      K"      L"      M"      N"      O"      P"      Q"      R"      S"      T"      U"      V"      W"      X"      Y"      Z"      ["      \"      ]"      ^"      _"      `"      a"      b"      c"      d"      e"      f"      g"      h"      i"      j"      k"      l"      m"      n"      o"      p"      q"      r"      s"      t"      u"      v"      w"      x"      y"      z"      {"      |"      }"      ~"      "      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"       #      #      #      #      #      #      #      #      #      	#      
#      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #       #      !#      "#      ##      $#      %#      &#      '#      (#      )#      *#      +#      ,#      -#      .#      /#      0#      1#      2#      3#      4#      5#      6#      7#      8#      9#      :#      ;#      <#      =#      >#      ?#      @#      A#      B#      C#      D#      E#      F#      G#      H#      I#      J#      K#      L#      M#      N#      O#      P#      Q#      R#      S#      T#      U#      V#      W#      X#      Y#      Z#      [#      \#      ]#      ^#      _#      `#      a#      b#      c#      d#      e#      f#      g#      h#      i#      j#      k#      l#      m#      n#      o#      p#      q#      r#      s#      t#      u#      v#      w#      x#      y#      z#      {#      |#      }#      ~#      #      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#       $      $      $      $      $      $      $      $      $      	$      
$      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $       $      !$      "$      #$      $$      %$      &$      '$      ($      )$      *$      +$      ,$      -$      .$      /$      0$      1$      2$      3$      4$      5$      6$      7$      8$      9$      :$      ;$      <$      =$      >$      ?$      @$      A$      B$      C$      D$      E$      F$      G$      H$      I$      J$      K$      L$      M$      N$      O$      P$      Q$      R$      S$      T$      U$      V$      W$      X$      Y$      Z$      [$      \$      ]$      ^$      _$      `$      a$      b$      c$      d$      e$      f$      g$      h$      i$      j$      k$      l$      m$      n$      o$      p$      q$      r$      s$      t$      u$      v$      w$      x$      y$      z$      {$      |$      }$      ~$      $      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$       %      %      %      %      %      %      %      %      %      	%      
%      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %       %      !%      "%      #%      $%      %%      &%      '%      (%      )%      *%      +%      ,%      -%      .%      /%      0%      1%      2%      3%      4%      5%      6%      7%      8%      9%      :%      ;%      <%      =%      >%      ?%      @%      A%      B%      C%      D%      E%      F%      G%      H%      I%      J%      K%      L%      M%      N%      O%      P%      Q%      R%      S%      T%      U%      V%      W%      X%      Y%      Z%      [%      \%      ]%      ^%      _%      `%      a%      b%      c%      d%      e%      f%      g%      h%      i%      j%      k%      l%      m%      n%      o%      p%      q%      r%      s%      t%      u%      v%      w%      x%      y%      z%      {%      |%      }%      ~%      %      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%       &      &      &      &      &      &      &      &      &      	&      
&      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &       &      !&      "&      #&      $&      %&      &&      '&      (&      )&      *&      +&      ,&      -&      .&      /&      0&      1&      2&      3&      4&      5&      6&      7&      8&      9&      :&      ;&      <&      =&      >&      ?&      @&      A&      B&      C&      D&      E&      F&      G&      H&      I&      J&      K&      L&      M&      N&      O&      P&      Q&      R&      S&      T&      U&      V&      W&      X&      Y&      Z&      [&      \&      ]&      ^&      _&      `&      a&      b&      c&      d&      e&      f&      g&      h&      i&      j&      k&      l&      m&      n&      o&      p&      q&      r&      s&      t&      u&      v&      w&      x&      y&      z&      {&      |&      }&      ~&      &      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&       '      '      '      '      '      '      '      '      '      	'      
'      '      '      '      '      '      
�
StatefulPartitionedCallStatefulPartitionedCall
hash_tableConst_4Const_5*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *$
fR
__inference_<lambda>_234173
�
PartitionedCallPartitionedCall*	
Tin
 *
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *$
fR
__inference_<lambda>_234178
8
NoOpNoOp^PartitionedCall^StatefulPartitionedCall
�
?MutableHashTable_lookup_table_export_values/LookupTableExportV2LookupTableExportV2MutableHashTable*
Tkeys0*
Tvalues0	*#
_class
loc:@MutableHashTable*
_output_shapes

::
�C
Const_6Const"/device:CPU:0*
_output_shapes
: *
dtype0*�B
value�BB�B B�B
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer-2
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*	&call_and_return_all_conditional_losses

_default_save_signature
	optimizer

signatures*
;
	keras_api
_lookup_layer
_adapt_function*
�
layer_with_weights-0
layer-0
layer-1
layer-2
layer-3
layer_with_weights-1
layer-4
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
	optimizer*
�
	variables
trainable_variables
regularization_losses
	keras_api
 __call__
*!&call_and_return_all_conditional_losses* 

"1
#2
$3*

"0
#1
$2*
* 
�
%non_trainable_variables

&layers
'metrics
(layer_regularization_losses
)layer_metrics
	variables
trainable_variables
regularization_losses
__call__

_default_save_signature
*	&call_and_return_all_conditional_losses
&	"call_and_return_conditional_losses*
6
*trace_0
+trace_1
,trace_2
-trace_3* 
6
.trace_0
/trace_1
0trace_2
1trace_3* 
* 
* 

2serving_default* 
* 
7
3	keras_api
4lookup_table
5token_counts*

6trace_0* 
�
7	variables
8trainable_variables
9regularization_losses
:	keras_api
;__call__
*<&call_and_return_all_conditional_losses
"
embeddings*
�
=	variables
>trainable_variables
?regularization_losses
@	keras_api
A__call__
*B&call_and_return_all_conditional_losses
C_random_generator* 
�
D	variables
Etrainable_variables
Fregularization_losses
G	keras_api
H__call__
*I&call_and_return_all_conditional_losses* 
�
J	variables
Ktrainable_variables
Lregularization_losses
M	keras_api
N__call__
*O&call_and_return_all_conditional_losses
P_random_generator* 
�
Q	variables
Rtrainable_variables
Sregularization_losses
T	keras_api
U__call__
*V&call_and_return_all_conditional_losses

#kernel
$bias*

"0
#1
$2*

"0
#1
$2*
* 
�
Wnon_trainable_variables

Xlayers
Ymetrics
Zlayer_regularization_losses
[layer_metrics
	variables
trainable_variables
regularization_losses
__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses*
P
\trace_0
]trace_1
^trace_2
_trace_3
`trace_4
atrace_5* 
P
btrace_0
ctrace_1
dtrace_2
etrace_3
ftrace_4
gtrace_5* 
�
hiter

ibeta_1

jbeta_2
	kdecay
llearning_rate"m�#m�$m�"v�#v�$v�*
* 
* 
* 
�
mnon_trainable_variables

nlayers
ometrics
player_regularization_losses
qlayer_metrics
	variables
trainable_variables
regularization_losses
 __call__
*!&call_and_return_all_conditional_losses
&!"call_and_return_conditional_losses* 

rtrace_0* 

strace_0* 
TN
VARIABLE_VALUEembedding/embeddings&variables/1/.ATTRIBUTES/VARIABLE_VALUE*
LF
VARIABLE_VALUEdense/kernel&variables/2/.ATTRIBUTES/VARIABLE_VALUE*
JD
VARIABLE_VALUE
dense/bias&variables/3/.ATTRIBUTES/VARIABLE_VALUE*
* 

0
1
2*

t0
u1*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
R
v_initializer
w_create_resource
x_initialize
y_destroy_resource* 
�
z_create_resource
{_initialize
|_destroy_resource><layer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/*
* 

"0*

"0*
* 
�
}non_trainable_variables

~layers
metrics
 �layer_regularization_losses
�layer_metrics
7	variables
8trainable_variables
9regularization_losses
;__call__
*<&call_and_return_all_conditional_losses
&<"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
=	variables
>trainable_variables
?regularization_losses
A__call__
*B&call_and_return_all_conditional_losses
&B"call_and_return_conditional_losses* 

�trace_0
�trace_1* 

�trace_0
�trace_1* 
* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
D	variables
Etrainable_variables
Fregularization_losses
H__call__
*I&call_and_return_all_conditional_losses
&I"call_and_return_conditional_losses* 

�trace_0* 

�trace_0* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
J	variables
Ktrainable_variables
Lregularization_losses
N__call__
*O&call_and_return_all_conditional_losses
&O"call_and_return_conditional_losses* 

�trace_0
�trace_1* 

�trace_0
�trace_1* 
* 

#0
$1*

#0
$1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
Q	variables
Rtrainable_variables
Sregularization_losses
U__call__
*V&call_and_return_all_conditional_losses
&V"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
* 
'
0
1
2
3
4*

�0
�1*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
a[
VARIABLE_VALUE	Adam/iter>layer_with_weights-1/optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE*
e_
VARIABLE_VALUEAdam/beta_1@layer_with_weights-1/optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE*
e_
VARIABLE_VALUEAdam/beta_2@layer_with_weights-1/optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE*
c]
VARIABLE_VALUE
Adam/decay?layer_with_weights-1/optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE*
sm
VARIABLE_VALUEAdam/learning_rateGlayer_with_weights-1/optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE*
* 
* 
* 
* 
* 
* 
* 
<
�	variables
�	keras_api

�total

�count*
M
�	variables
�	keras_api

�total

�count
�
_fn_kwargs*
* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
<
�	variables
�	keras_api

�total

�count*
M
�	variables
�	keras_api

�total

�count
�
_fn_kwargs*

�0
�1*

�	variables*
UO
VARIABLE_VALUEtotal_34keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE*
UO
VARIABLE_VALUEcount_34keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE*

�0
�1*

�	variables*
UO
VARIABLE_VALUEtotal_24keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE*
UO
VARIABLE_VALUEcount_24keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE*
* 
* 
* 
* 
* 
* 
* 

�0
�1*

�	variables*
jd
VARIABLE_VALUEtotal_1Ilayer_with_weights-1/keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE*
jd
VARIABLE_VALUEcount_1Ilayer_with_weights-1/keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE*

�0
�1*

�	variables*
hb
VARIABLE_VALUEtotalIlayer_with_weights-1/keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE*
hb
VARIABLE_VALUEcountIlayer_with_weights-1/keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE*
* 
��
VARIABLE_VALUEAdam/embedding/embeddings/mWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�~
VARIABLE_VALUEAdam/dense/kernel/mWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�|
VARIABLE_VALUEAdam/dense/bias/mWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
��
VARIABLE_VALUEAdam/embedding/embeddings/vWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�~
VARIABLE_VALUEAdam/dense/kernel/vWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�|
VARIABLE_VALUEAdam/dense/bias/vWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�
(serving_default_text_vectorization_inputPlaceholder*#
_output_shapes
:���������*
dtype0*
shape:���������
�
StatefulPartitionedCall_1StatefulPartitionedCall(serving_default_text_vectorization_input
hash_tableConstConst_1Const_2embedding/embeddingsdense/kernel
dense/bias*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *-
f(R&
$__inference_signature_wrapper_233594
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�	
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filename(embedding/embeddings/Read/ReadVariableOp dense/kernel/Read/ReadVariableOpdense/bias/Read/ReadVariableOp?MutableHashTable_lookup_table_export_values/LookupTableExportV2AMutableHashTable_lookup_table_export_values/LookupTableExportV2:1Adam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal_3/Read/ReadVariableOpcount_3/Read/ReadVariableOptotal_2/Read/ReadVariableOpcount_2/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp/Adam/embedding/embeddings/m/Read/ReadVariableOp'Adam/dense/kernel/m/Read/ReadVariableOp%Adam/dense/bias/m/Read/ReadVariableOp/Adam/embedding/embeddings/v/Read/ReadVariableOp'Adam/dense/kernel/v/Read/ReadVariableOp%Adam/dense/bias/v/Read/ReadVariableOpConst_6*%
Tin
2		*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *(
f#R!
__inference__traced_save_234281
�
StatefulPartitionedCall_3StatefulPartitionedCallsaver_filenameembedding/embeddingsdense/kernel
dense/biasMutableHashTable	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotal_3count_3total_2count_2total_1count_1totalcountAdam/embedding/embeddings/mAdam/dense/kernel/mAdam/dense/bias/mAdam/embedding/embeddings/vAdam/dense/kernel/vAdam/dense/bias/v*#
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *+
f&R$
"__inference__traced_restore_234360��
�
�
+__inference_sequential_layer_call_fn_233862

inputs
unknown:	�N
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233115o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�
+
__inference_<lambda>_234178
identityJ
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
�
F__inference_sequential_layer_call_and_return_conditional_losses_233014

inputs#
embedding_232979:	�N
dense_233008:
dense_233010:
identity��dense/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�
!embedding/StatefulPartitionedCallStatefulPartitionedCallinputsembedding_232979*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_232978�
dropout/PartitionedCallPartitionedCall*embedding/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_dropout_layer_call_and_return_conditional_losses_232987�
(global_average_pooling1d/PartitionedCallPartitionedCall dropout/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *]
fXRV
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_232958�
dropout_1/PartitionedCallPartitionedCall1global_average_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_232995�
dense/StatefulPartitionedCallStatefulPartitionedCall"dropout_1/PartitionedCall:output:0dense_233008dense_233010*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *J
fERC
A__inference_dense_layer_call_and_return_conditional_losses_233007u
IdentityIdentity&dense/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/StatefulPartitionedCall"^embedding/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
��
�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233840

inputsO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	?
,sequential_embedding_embedding_lookup_233809:	�NA
/sequential_dense_matmul_readvariableop_resource:>
0sequential_dense_biasadd_readvariableop_resource:
identity��'sequential/dense/BiasAdd/ReadVariableOp�&sequential/dense/MatMul/ReadVariableOp�%sequential/embedding/embedding_lookup�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Z
text_vectorization/StringLowerStringLowerinputs*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*
pattern<br />*
rewrite �
'text_vectorization/StaticRegexReplace_1StaticRegexReplace.text_vectorization/StaticRegexReplace:output:0*#
_output_shapes
:���������*A
pattern64[!"\#\$%\&'\(\)\*\+,\-\./:;<=>\?@\[\\\]\^_`\{\|\}\~]*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV20text_vectorization/StaticRegexReplace_1:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
sequential/CastCast?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*

DstT0*

SrcT0	*(
_output_shapes
:����������x
sequential/embedding/CastCastsequential/Cast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
%sequential/embedding/embedding_lookupResourceGather,sequential_embedding_embedding_lookup_233809sequential/embedding/Cast:y:0*
Tindices0*?
_class5
31loc:@sequential/embedding/embedding_lookup/233809*,
_output_shapes
:����������*
dtype0�
.sequential/embedding/embedding_lookup/IdentityIdentity.sequential/embedding/embedding_lookup:output:0*
T0*?
_class5
31loc:@sequential/embedding/embedding_lookup/233809*,
_output_shapes
:�����������
0sequential/embedding/embedding_lookup/Identity_1Identity7sequential/embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������e
 sequential/dropout/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
sequential/dropout/dropout/MulMul9sequential/embedding/embedding_lookup/Identity_1:output:0)sequential/dropout/dropout/Const:output:0*
T0*,
_output_shapes
:�����������
 sequential/dropout/dropout/ShapeShape9sequential/embedding/embedding_lookup/Identity_1:output:0*
T0*
_output_shapes
:�
7sequential/dropout/dropout/random_uniform/RandomUniformRandomUniform)sequential/dropout/dropout/Shape:output:0*
T0*,
_output_shapes
:����������*
dtype0n
)sequential/dropout/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
'sequential/dropout/dropout/GreaterEqualGreaterEqual@sequential/dropout/dropout/random_uniform/RandomUniform:output:02sequential/dropout/dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:�����������
sequential/dropout/dropout/CastCast+sequential/dropout/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:�����������
 sequential/dropout/dropout/Mul_1Mul"sequential/dropout/dropout/Mul:z:0#sequential/dropout/dropout/Cast:y:0*
T0*,
_output_shapes
:����������|
:sequential/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
(sequential/global_average_pooling1d/MeanMean$sequential/dropout/dropout/Mul_1:z:0Csequential/global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������g
"sequential/dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
 sequential/dropout_1/dropout/MulMul1sequential/global_average_pooling1d/Mean:output:0+sequential/dropout_1/dropout/Const:output:0*
T0*'
_output_shapes
:����������
"sequential/dropout_1/dropout/ShapeShape1sequential/global_average_pooling1d/Mean:output:0*
T0*
_output_shapes
:�
9sequential/dropout_1/dropout/random_uniform/RandomUniformRandomUniform+sequential/dropout_1/dropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0p
+sequential/dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
)sequential/dropout_1/dropout/GreaterEqualGreaterEqualBsequential/dropout_1/dropout/random_uniform/RandomUniform:output:04sequential/dropout_1/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:����������
!sequential/dropout_1/dropout/CastCast-sequential/dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:����������
"sequential/dropout_1/dropout/Mul_1Mul$sequential/dropout_1/dropout/Mul:z:0%sequential/dropout_1/dropout/Cast:y:0*
T0*'
_output_shapes
:����������
&sequential/dense/MatMul/ReadVariableOpReadVariableOp/sequential_dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
sequential/dense/MatMulMatMul&sequential/dropout_1/dropout/Mul_1:z:0.sequential/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
'sequential/dense/BiasAdd/ReadVariableOpReadVariableOp0sequential_dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential/dense/BiasAddBiasAdd!sequential/dense/MatMul:product:0/sequential/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������t
activation_1/SigmoidSigmoid!sequential/dense/BiasAdd:output:0*
T0*'
_output_shapes
:���������g
IdentityIdentityactivation_1/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp(^sequential/dense/BiasAdd/ReadVariableOp'^sequential/dense/MatMul/ReadVariableOp&^sequential/embedding/embedding_lookup?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2R
'sequential/dense/BiasAdd/ReadVariableOp'sequential/dense/BiasAdd/ReadVariableOp2P
&sequential/dense/MatMul/ReadVariableOp&sequential/dense/MatMul/ReadVariableOp2N
%sequential/embedding/embedding_lookup%sequential/embedding/embedding_lookup2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�	
�
$__inference_signature_wrapper_233594
text_vectorization_input
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�N
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalltext_vectorization_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� **
f%R#
!__inference__wrapped_model_232948o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
F
*__inference_dropout_1_layer_call_fn_234064

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_232995`
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
__inference_<lambda>_2341737
3key_value_init1441_lookuptableimportv2_table_handle/
+key_value_init1441_lookuptableimportv2_keys1
-key_value_init1441_lookuptableimportv2_values	
identity��&key_value_init1441/LookupTableImportV2�
&key_value_init1441/LookupTableImportV2LookupTableImportV23key_value_init1441_lookuptableimportv2_table_handle+key_value_init1441_lookuptableimportv2_keys-key_value_init1441_lookuptableimportv2_values*	
Tin0*

Tout0	*
_output_shapes
 J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: o
NoOpNoOp'^key_value_init1441/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
: :�N:�N2P
&key_value_init1441/LookupTableImportV2&key_value_init1441/LookupTableImportV2:!

_output_shapes	
:�N:!

_output_shapes	
:�N
�
d
H__inference_activation_1_layer_call_and_return_conditional_losses_233253

inputs
identityL
SigmoidSigmoidinputs*
T0*'
_output_shapes
:���������S
IdentityIdentitySigmoid:y:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
;
__inference__creator_234110
identity��
hash_tablel

hash_tableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name1442*
value_dtype0	W
IdentityIdentityhash_table:table_handle:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp^hash_table*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 2

hash_table
hash_table
�	
�
E__inference_embedding_layer_call_and_return_conditional_losses_232978

inputs*
embedding_lookup_232972:	�N
identity��embedding_lookup^
CastCastinputs*

DstT0*

SrcT0*0
_output_shapes
:�������������������
embedding_lookupResourceGatherembedding_lookup_232972Cast:y:0*
Tindices0**
_class 
loc:@embedding_lookup/232972*4
_output_shapes"
 :������������������*
dtype0�
embedding_lookup/IdentityIdentityembedding_lookup:output:0*
T0**
_class 
loc:@embedding_lookup/232972*4
_output_shapes"
 :�������������������
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*4
_output_shapes"
 :�������������������
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*4
_output_shapes"
 :������������������Y
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:������������������: 2$
embedding_lookupembedding_lookup:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�
p
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_232958

inputs
identityX
Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :p
MeanMeaninputsMean/reduction_indices:output:0*
T0*0
_output_shapes
:������������������^
IdentityIdentityMean:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�_
�
"__inference__traced_restore_234360
file_prefix8
%assignvariableop_embedding_embeddings:	�N1
assignvariableop_1_dense_kernel:+
assignvariableop_2_dense_bias:M
Cmutablehashtable_table_restore_lookuptableimportv2_mutablehashtable: &
assignvariableop_3_adam_iter:	 (
assignvariableop_4_adam_beta_1: (
assignvariableop_5_adam_beta_2: '
assignvariableop_6_adam_decay: /
%assignvariableop_7_adam_learning_rate: $
assignvariableop_8_total_3: $
assignvariableop_9_count_3: %
assignvariableop_10_total_2: %
assignvariableop_11_count_2: %
assignvariableop_12_total_1: %
assignvariableop_13_count_1: #
assignvariableop_14_total: #
assignvariableop_15_count: B
/assignvariableop_16_adam_embedding_embeddings_m:	�N9
'assignvariableop_17_adam_dense_kernel_m:3
%assignvariableop_18_adam_dense_bias_m:B
/assignvariableop_19_adam_embedding_embeddings_v:	�N9
'assignvariableop_20_adam_dense_kernel_v:3
%assignvariableop_21_adam_dense_bias_v:
identity_23��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�2MutableHashTable_table_restore/LookupTableImportV2�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEBFlayer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/table-keysBHlayer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/table-valuesB>layer_with_weights-1/optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-1/optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-1/optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-1/optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEBGlayer_with_weights-1/optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*x
_output_shapesf
d:::::::::::::::::::::::::*'
dtypes
2		[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOp%assignvariableop_embedding_embeddingsIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOpassignvariableop_1_dense_kernelIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOpassignvariableop_2_dense_biasIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype0�
2MutableHashTable_table_restore/LookupTableImportV2LookupTableImportV2Cmutablehashtable_table_restore_lookuptableimportv2_mutablehashtableRestoreV2:tensors:3RestoreV2:tensors:4*	
Tin0*

Tout0	*#
_class
loc:@MutableHashTable*
_output_shapes
 ]

Identity_3IdentityRestoreV2:tensors:5"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_3AssignVariableOpassignvariableop_3_adam_iterIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	]

Identity_4IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_4AssignVariableOpassignvariableop_4_adam_beta_1Identity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_5IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_5AssignVariableOpassignvariableop_5_adam_beta_2Identity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_6IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOpassignvariableop_6_adam_decayIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOp%assignvariableop_7_adam_learning_rateIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype0^

Identity_8IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_8AssignVariableOpassignvariableop_8_total_3Identity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0^

Identity_9IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOpassignvariableop_9_count_3Identity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOpassignvariableop_10_total_2Identity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOpassignvariableop_11_count_2Identity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_12AssignVariableOpassignvariableop_12_total_1Identity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_13IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_13AssignVariableOpassignvariableop_13_count_1Identity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_14AssignVariableOpassignvariableop_14_totalIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_15AssignVariableOpassignvariableop_15_countIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_16AssignVariableOp/assignvariableop_16_adam_embedding_embeddings_mIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_17IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_17AssignVariableOp'assignvariableop_17_adam_dense_kernel_mIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_18AssignVariableOp%assignvariableop_18_adam_dense_bias_mIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_19AssignVariableOp/assignvariableop_19_adam_embedding_embeddings_vIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_20IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_20AssignVariableOp'assignvariableop_20_adam_dense_kernel_vIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_21IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_21AssignVariableOp%assignvariableop_21_adam_dense_bias_vIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype01
NoOpNoOp"/device:CPU:0*
_output_shapes
 �
Identity_22Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_93^MutableHashTable_table_restore/LookupTableImportV2^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_23IdentityIdentity_22:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_93^MutableHashTable_table_restore/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "#
identity_23Identity_23:output:0*C
_input_shapes2
0: : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92h
2MutableHashTable_table_restore/LookupTableImportV22MutableHashTable_table_restore/LookupTableImportV2:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:)%
#
_class
loc:@MutableHashTable
�`
�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233513
text_vectorization_inputO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	$
sequential_233504:	�N#
sequential_233506:
sequential_233508:
identity��"sequential/StatefulPartitionedCall�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2l
text_vectorization/StringLowerStringLowertext_vectorization_input*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*
pattern<br />*
rewrite �
'text_vectorization/StaticRegexReplace_1StaticRegexReplace.text_vectorization/StaticRegexReplace:output:0*#
_output_shapes
:���������*A
pattern64[!"\#\$%\&'\(\)\*\+,\-\./:;<=>\?@\[\\\]\^_`\{\|\}\~]*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV20text_vectorization/StaticRegexReplace_1:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
"sequential/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0sequential_233504sequential_233506sequential_233508*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233240�
activation_1/PartitionedCallPartitionedCall+sequential/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_activation_1_layer_call_and_return_conditional_losses_233253t
IdentityIdentity%activation_1/PartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp#^sequential/StatefulPartitionedCall?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2H
"sequential/StatefulPartitionedCall"sequential/StatefulPartitionedCall2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
F__inference_sequential_layer_call_and_return_conditional_losses_233240

inputs	4
!embedding_embedding_lookup_233224:	�N6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookupV
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:����������b
embedding/CastCastCast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
embedding/embedding_lookupResourceGather!embedding_embedding_lookup_233224embedding/Cast:y:0*
Tindices0*4
_class*
(&loc:@embedding/embedding_lookup/233224*,
_output_shapes
:����������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*4
_class*
(&loc:@embedding/embedding_lookup/233224*,
_output_shapes
:�����������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:�����������
dropout/IdentityIdentity.embedding/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:����������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/Identity:output:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������x
dropout_1/IdentityIdentity&global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/Identity:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�	
�
-__inference_sequential_2_layer_call_fn_233667

inputs
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�N
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_sequential_2_layer_call_and_return_conditional_losses_233256o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�C
�
__inference_adapt_step_233642
iterator9
5none_lookup_table_find_lookuptablefindv2_table_handle:
6none_lookup_table_find_lookuptablefindv2_default_value	��IteratorGetNext�(None_lookup_table_find/LookupTableFindV2�,None_lookup_table_insert/LookupTableInsertV2�
IteratorGetNextIteratorGetNextiterator*
_class
loc:@iterator*#
_output_shapes
:���������*"
output_shapes
:���������*
output_types
2]
StringLowerStringLowerIteratorGetNext:components:0*#
_output_shapes
:����������
StaticRegexReplaceStaticRegexReplaceStringLower:output:0*#
_output_shapes
:���������*
pattern<br />*
rewrite �
StaticRegexReplace_1StaticRegexReplaceStaticRegexReplace:output:0*#
_output_shapes
:���������*A
pattern64[!"\#\$%\&'\(\)\*\+,\-\./:;<=>\?@\[\\\]\^_`\{\|\}\~]*
rewrite R
StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
StringSplit/StringSplitV2StringSplitV2StaticRegexReplace_1:output:0StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:p
StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        r
!StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       r
!StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
StringSplit/strided_sliceStridedSlice#StringSplit/StringSplitV2:indices:0(StringSplit/strided_slice/stack:output:0*StringSplit/strided_slice/stack_1:output:0*StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_maskk
!StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: m
#StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:m
#StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
StringSplit/strided_slice_1StridedSlice!StringSplit/StringSplitV2:shape:0*StringSplit/strided_slice_1/stack:output:0,StringSplit/strided_slice_1/stack_1:output:0,StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
BStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast"StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
DStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast$StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
LStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeFStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
LStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
KStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdUStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0UStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
PStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterTStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0YStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
KStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastRStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
JStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxFStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0WStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
LStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
JStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2SStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0UStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
JStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulOStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximumHStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimumHStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0RStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
OStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountFStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0RStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0WStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
IStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
DStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumVStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0RStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
MStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
IStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
DStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2VStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0JStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0RStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
UniqueWithCountsUniqueWithCounts"StringSplit/StringSplitV2:values:0*
T0*A
_output_shapes/
-:���������:���������:���������*
out_idx0	�
(None_lookup_table_find/LookupTableFindV2LookupTableFindV25none_lookup_table_find_lookuptablefindv2_table_handleUniqueWithCounts:y:06none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
:|
addAddV2UniqueWithCounts:count:01None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:�
,None_lookup_table_insert/LookupTableInsertV2LookupTableInsertV25none_lookup_table_find_lookuptablefindv2_table_handleUniqueWithCounts:y:0add:z:0)^None_lookup_table_find/LookupTableFindV2",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
 *(
_construction_contextkEagerRuntime*
_input_shapes
: : : 2"
IteratorGetNextIteratorGetNext2T
(None_lookup_table_find/LookupTableFindV2(None_lookup_table_find/LookupTableFindV22\
,None_lookup_table_insert/LookupTableInsertV2,None_lookup_table_insert/LookupTableInsertV2:( $
"
_user_specified_name
iterator:

_output_shapes
: 
�
�
F__inference_sequential_layer_call_and_return_conditional_losses_233115

inputs#
embedding_233103:	�N
dense_233109:
dense_233111:
identity��dense/StatefulPartitionedCall�dropout/StatefulPartitionedCall�!dropout_1/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�
!embedding/StatefulPartitionedCallStatefulPartitionedCallinputsembedding_233103*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_232978�
dropout/StatefulPartitionedCallStatefulPartitionedCall*embedding/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_dropout_layer_call_and_return_conditional_losses_233076�
(global_average_pooling1d/PartitionedCallPartitionedCall(dropout/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *]
fXRV
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_232958�
!dropout_1/StatefulPartitionedCallStatefulPartitionedCall1global_average_pooling1d/PartitionedCall:output:0 ^dropout/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_233053�
dense/StatefulPartitionedCallStatefulPartitionedCall*dropout_1/StatefulPartitionedCall:output:0dense_233109dense_233111*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *J
fERC
A__inference_dense_layer_call_and_return_conditional_losses_233007u
IdentityIdentity&dense/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/StatefulPartitionedCall ^dropout/StatefulPartitionedCall"^dropout_1/StatefulPartitionedCall"^embedding/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dropout/StatefulPartitionedCalldropout/StatefulPartitionedCall2F
!dropout_1/StatefulPartitionedCall!dropout_1/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�
p
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_234059

inputs
identityX
Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :p
MeanMeaninputsMean/reduction_indices:output:0*
T0*0
_output_shapes
:������������������^
IdentityIdentityMean:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�	
�
-__inference_sequential_2_layer_call_fn_233453
text_vectorization_input
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�N
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalltext_vectorization_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_sequential_2_layer_call_and_return_conditional_losses_233417o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�~
�
!__inference__wrapped_model_232948
text_vectorization_input\
Xsequential_2_text_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle]
Ysequential_2_text_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	9
5sequential_2_text_vectorization_string_lookup_equal_y<
8sequential_2_text_vectorization_string_lookup_selectv2_t	L
9sequential_2_sequential_embedding_embedding_lookup_232931:	�NN
<sequential_2_sequential_dense_matmul_readvariableop_resource:K
=sequential_2_sequential_dense_biasadd_readvariableop_resource:
identity��4sequential_2/sequential/dense/BiasAdd/ReadVariableOp�3sequential_2/sequential/dense/MatMul/ReadVariableOp�2sequential_2/sequential/embedding/embedding_lookup�Ksequential_2/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2y
+sequential_2/text_vectorization/StringLowerStringLowertext_vectorization_input*#
_output_shapes
:����������
2sequential_2/text_vectorization/StaticRegexReplaceStaticRegexReplace4sequential_2/text_vectorization/StringLower:output:0*#
_output_shapes
:���������*
pattern<br />*
rewrite �
4sequential_2/text_vectorization/StaticRegexReplace_1StaticRegexReplace;sequential_2/text_vectorization/StaticRegexReplace:output:0*#
_output_shapes
:���������*A
pattern64[!"\#\$%\&'\(\)\*\+,\-\./:;<=>\?@\[\\\]\^_`\{\|\}\~]*
rewrite r
1sequential_2/text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
9sequential_2/text_vectorization/StringSplit/StringSplitV2StringSplitV2=sequential_2/text_vectorization/StaticRegexReplace_1:output:0:sequential_2/text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
?sequential_2/text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
Asequential_2/text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
Asequential_2/text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
9sequential_2/text_vectorization/StringSplit/strided_sliceStridedSliceCsequential_2/text_vectorization/StringSplit/StringSplitV2:indices:0Hsequential_2/text_vectorization/StringSplit/strided_slice/stack:output:0Jsequential_2/text_vectorization/StringSplit/strided_slice/stack_1:output:0Jsequential_2/text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
Asequential_2/text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
Csequential_2/text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
Csequential_2/text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
;sequential_2/text_vectorization/StringSplit/strided_slice_1StridedSliceAsequential_2/text_vectorization/StringSplit/StringSplitV2:shape:0Jsequential_2/text_vectorization/StringSplit/strided_slice_1/stack:output:0Lsequential_2/text_vectorization/StringSplit/strided_slice_1/stack_1:output:0Lsequential_2/text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
bsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCastBsequential_2/text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
dsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1CastDsequential_2/text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
lsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapefsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
lsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
ksequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdusequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0usequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
psequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
nsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatertsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ysequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
ksequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastrsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
nsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
jsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxfsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0wsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
lsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
jsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ssequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0usequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
jsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulosequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0nsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
nsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximumhsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0nsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
nsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimumhsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0rsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
nsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
osequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountfsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0rsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0wsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
isequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
dsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumvsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0rsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
msequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
isequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
dsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2vsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0jsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0rsequential_2/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Ksequential_2/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Xsequential_2_text_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleBsequential_2/text_vectorization/StringSplit/StringSplitV2:values:0Ysequential_2_text_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
3sequential_2/text_vectorization/string_lookup/EqualEqualBsequential_2/text_vectorization/StringSplit/StringSplitV2:values:05sequential_2_text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
6sequential_2/text_vectorization/string_lookup/SelectV2SelectV27sequential_2/text_vectorization/string_lookup/Equal:z:08sequential_2_text_vectorization_string_lookup_selectv2_tTsequential_2/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
6sequential_2/text_vectorization/string_lookup/IdentityIdentity?sequential_2/text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������~
<sequential_2/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
4sequential_2/text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
Csequential_2/text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor=sequential_2/text_vectorization/RaggedToTensor/Const:output:0?sequential_2/text_vectorization/string_lookup/Identity:output:0Esequential_2/text_vectorization/RaggedToTensor/default_value:output:0Dsequential_2/text_vectorization/StringSplit/strided_slice_1:output:0Bsequential_2/text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
sequential_2/sequential/CastCastLsequential_2/text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*

DstT0*

SrcT0	*(
_output_shapes
:�����������
&sequential_2/sequential/embedding/CastCast sequential_2/sequential/Cast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
2sequential_2/sequential/embedding/embedding_lookupResourceGather9sequential_2_sequential_embedding_embedding_lookup_232931*sequential_2/sequential/embedding/Cast:y:0*
Tindices0*L
_classB
@>loc:@sequential_2/sequential/embedding/embedding_lookup/232931*,
_output_shapes
:����������*
dtype0�
;sequential_2/sequential/embedding/embedding_lookup/IdentityIdentity;sequential_2/sequential/embedding/embedding_lookup:output:0*
T0*L
_classB
@>loc:@sequential_2/sequential/embedding/embedding_lookup/232931*,
_output_shapes
:�����������
=sequential_2/sequential/embedding/embedding_lookup/Identity_1IdentityDsequential_2/sequential/embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:�����������
(sequential_2/sequential/dropout/IdentityIdentityFsequential_2/sequential/embedding/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:�����������
Gsequential_2/sequential/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
5sequential_2/sequential/global_average_pooling1d/MeanMean1sequential_2/sequential/dropout/Identity:output:0Psequential_2/sequential/global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:����������
*sequential_2/sequential/dropout_1/IdentityIdentity>sequential_2/sequential/global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
3sequential_2/sequential/dense/MatMul/ReadVariableOpReadVariableOp<sequential_2_sequential_dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
$sequential_2/sequential/dense/MatMulMatMul3sequential_2/sequential/dropout_1/Identity:output:0;sequential_2/sequential/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
4sequential_2/sequential/dense/BiasAdd/ReadVariableOpReadVariableOp=sequential_2_sequential_dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
%sequential_2/sequential/dense/BiasAddBiasAdd.sequential_2/sequential/dense/MatMul:product:0<sequential_2/sequential/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
!sequential_2/activation_1/SigmoidSigmoid.sequential_2/sequential/dense/BiasAdd:output:0*
T0*'
_output_shapes
:���������t
IdentityIdentity%sequential_2/activation_1/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp5^sequential_2/sequential/dense/BiasAdd/ReadVariableOp4^sequential_2/sequential/dense/MatMul/ReadVariableOp3^sequential_2/sequential/embedding/embedding_lookupL^sequential_2/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2l
4sequential_2/sequential/dense/BiasAdd/ReadVariableOp4sequential_2/sequential/dense/BiasAdd/ReadVariableOp2j
3sequential_2/sequential/dense/MatMul/ReadVariableOp3sequential_2/sequential/dense/MatMul/ReadVariableOp2h
2sequential_2/sequential/embedding/embedding_lookup2sequential_2/sequential/embedding/embedding_lookup2�
Ksequential_2/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Ksequential_2/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
+__inference_sequential_layer_call_fn_233135
embedding_input
unknown:	�N
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallembedding_inputunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233115o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:a ]
0
_output_shapes
:������������������
)
_user_specified_nameembedding_input
�
�
+__inference_sequential_layer_call_fn_233884

inputs	
unknown:	�N
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233327o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
U
9__inference_global_average_pooling1d_layer_call_fn_234053

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *]
fXRV
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_232958i
IdentityIdentityPartitionedCall:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�	
�
A__inference_dense_layer_call_and_return_conditional_losses_233007

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������_
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:���������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
F__inference_sequential_layer_call_and_return_conditional_losses_233959

inputs	4
!embedding_embedding_lookup_233943:	�N6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookupV
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:����������b
embedding/CastCastCast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
embedding/embedding_lookupResourceGather!embedding_embedding_lookup_233943embedding/Cast:y:0*
Tindices0*4
_class*
(&loc:@embedding/embedding_lookup/233943*,
_output_shapes
:����������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*4
_class*
(&loc:@embedding/embedding_lookup/233943*,
_output_shapes
:�����������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:�����������
dropout/IdentityIdentity.embedding/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:����������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/Identity:output:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������x
dropout_1/IdentityIdentity&global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/Identity:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
c
E__inference_dropout_1_layer_call_and_return_conditional_losses_234074

inputs

identity_1N
IdentityIdentityinputs*
T0*'
_output_shapes
:���������[

Identity_1IdentityIdentity:output:0*
T0*'
_output_shapes
:���������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
+__inference_sequential_layer_call_fn_233851

inputs
unknown:	�N
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233014o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�	
d
E__inference_dropout_1_layer_call_and_return_conditional_losses_233053

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?d
dropout/MulMulinputsdropout/Const:output:0*
T0*'
_output_shapes
:���������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:���������o
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:���������i
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*'
_output_shapes
:���������Y
IdentityIdentitydropout/Mul_1:z:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
+__inference_sequential_layer_call_fn_233873

inputs	
unknown:	�N
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233240o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
G
__inference__creator_234128
identity: ��MutableHashTable}
MutableHashTableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name
table_94*
value_dtype0	]
IdentityIdentityMutableHashTable:table_handle:0^NoOp*
T0*
_output_shapes
: Y
NoOpNoOp^MutableHashTable*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 2$
MutableHashTableMutableHashTable
�%
�
F__inference_sequential_layer_call_and_return_conditional_losses_233938

inputs4
!embedding_embedding_lookup_233908:	�N6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookuph
embedding/CastCastinputs*

DstT0*

SrcT0*0
_output_shapes
:�������������������
embedding/embedding_lookupResourceGather!embedding_embedding_lookup_233908embedding/Cast:y:0*
Tindices0*4
_class*
(&loc:@embedding/embedding_lookup/233908*4
_output_shapes"
 :������������������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*4
_class*
(&loc:@embedding/embedding_lookup/233908*4
_output_shapes"
 :�������������������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*4
_output_shapes"
 :������������������Z
dropout/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout/dropout/MulMul.embedding/embedding_lookup/Identity_1:output:0dropout/dropout/Const:output:0*
T0*4
_output_shapes"
 :������������������s
dropout/dropout/ShapeShape.embedding/embedding_lookup/Identity_1:output:0*
T0*
_output_shapes
:�
,dropout/dropout/random_uniform/RandomUniformRandomUniformdropout/dropout/Shape:output:0*
T0*4
_output_shapes"
 :������������������*
dtype0c
dropout/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/dropout/GreaterEqualGreaterEqual5dropout/dropout/random_uniform/RandomUniform:output:0'dropout/dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :�������������������
dropout/dropout/CastCast dropout/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :�������������������
dropout/dropout/Mul_1Muldropout/dropout/Mul:z:0dropout/dropout/Cast:y:0*
T0*4
_output_shapes"
 :������������������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/dropout/Mul_1:z:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������\
dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout_1/dropout/MulMul&global_average_pooling1d/Mean:output:0 dropout_1/dropout/Const:output:0*
T0*'
_output_shapes
:���������m
dropout_1/dropout/ShapeShape&global_average_pooling1d/Mean:output:0*
T0*
_output_shapes
:�
.dropout_1/dropout/random_uniform/RandomUniformRandomUniform dropout_1/dropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0e
 dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout_1/dropout/GreaterEqualGreaterEqual7dropout_1/dropout/random_uniform/RandomUniform:output:0)dropout_1/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:����������
dropout_1/dropout/CastCast"dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:����������
dropout_1/dropout/Mul_1Muldropout_1/dropout/Mul:z:0dropout_1/dropout/Cast:y:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/dropout/Mul_1:z:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�

b
C__inference_dropout_layer_call_and_return_conditional_losses_234048

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?q
dropout/MulMulinputsdropout/Const:output:0*
T0*4
_output_shapes"
 :������������������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*4
_output_shapes"
 :������������������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :������������������|
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :������������������v
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*4
_output_shapes"
 :������������������f
IdentityIdentitydropout/Mul_1:z:0*
T0*4
_output_shapes"
 :������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�	
�
E__inference_embedding_layer_call_and_return_conditional_losses_234021

inputs*
embedding_lookup_234015:	�N
identity��embedding_lookup^
CastCastinputs*

DstT0*

SrcT0*0
_output_shapes
:�������������������
embedding_lookupResourceGatherembedding_lookup_234015Cast:y:0*
Tindices0**
_class 
loc:@embedding_lookup/234015*4
_output_shapes"
 :������������������*
dtype0�
embedding_lookup/IdentityIdentityembedding_lookup:output:0*
T0**
_class 
loc:@embedding_lookup/234015*4
_output_shapes"
 :�������������������
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*4
_output_shapes"
 :�������������������
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*4
_output_shapes"
 :������������������Y
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:������������������: 2$
embedding_lookupembedding_lookup:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�	
d
E__inference_dropout_1_layer_call_and_return_conditional_losses_234086

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?d
dropout/MulMulinputsdropout/Const:output:0*
T0*'
_output_shapes
:���������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:���������o
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:���������i
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*'
_output_shapes
:���������Y
IdentityIdentitydropout/Mul_1:z:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
__inference__initializer_2341187
3key_value_init1441_lookuptableimportv2_table_handle/
+key_value_init1441_lookuptableimportv2_keys1
-key_value_init1441_lookuptableimportv2_values	
identity��&key_value_init1441/LookupTableImportV2�
&key_value_init1441/LookupTableImportV2LookupTableImportV23key_value_init1441_lookuptableimportv2_table_handle+key_value_init1441_lookuptableimportv2_keys-key_value_init1441_lookuptableimportv2_values*	
Tin0*

Tout0	*
_output_shapes
 G
ConstConst*
_output_shapes
: *
dtype0*
value	B :L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: o
NoOpNoOp'^key_value_init1441/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
: :�N:�N2P
&key_value_init1441/LookupTableImportV2&key_value_init1441/LookupTableImportV2:!

_output_shapes	
:�N:!

_output_shapes	
:�N
�
a
(__inference_dropout_layer_call_fn_234031

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_dropout_layer_call_and_return_conditional_losses_233076|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*4
_output_shapes"
 :������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������22
StatefulPartitionedCallStatefulPartitionedCall:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�`
�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233573
text_vectorization_inputO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	$
sequential_233564:	�N#
sequential_233566:
sequential_233568:
identity��"sequential/StatefulPartitionedCall�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2l
text_vectorization/StringLowerStringLowertext_vectorization_input*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*
pattern<br />*
rewrite �
'text_vectorization/StaticRegexReplace_1StaticRegexReplace.text_vectorization/StaticRegexReplace:output:0*#
_output_shapes
:���������*A
pattern64[!"\#\$%\&'\(\)\*\+,\-\./:;<=>\?@\[\\\]\^_`\{\|\}\~]*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV20text_vectorization/StaticRegexReplace_1:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
"sequential/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0sequential_233564sequential_233566sequential_233568*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233327�
activation_1/PartitionedCallPartitionedCall+sequential/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_activation_1_layer_call_and_return_conditional_losses_233253t
IdentityIdentity%activation_1/PartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp#^sequential/StatefulPartitionedCall?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2H
"sequential/StatefulPartitionedCall"sequential/StatefulPartitionedCall2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
D
(__inference_dropout_layer_call_fn_234026

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_dropout_layer_call_and_return_conditional_losses_232987m
IdentityIdentityPartitionedCall:output:0*
T0*4
_output_shapes"
 :������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�%
�
F__inference_sequential_layer_call_and_return_conditional_losses_233327

inputs	4
!embedding_embedding_lookup_233297:	�N6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookupV
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:����������b
embedding/CastCastCast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
embedding/embedding_lookupResourceGather!embedding_embedding_lookup_233297embedding/Cast:y:0*
Tindices0*4
_class*
(&loc:@embedding/embedding_lookup/233297*,
_output_shapes
:����������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*4
_class*
(&loc:@embedding/embedding_lookup/233297*,
_output_shapes
:�����������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������Z
dropout/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout/dropout/MulMul.embedding/embedding_lookup/Identity_1:output:0dropout/dropout/Const:output:0*
T0*,
_output_shapes
:����������s
dropout/dropout/ShapeShape.embedding/embedding_lookup/Identity_1:output:0*
T0*
_output_shapes
:�
,dropout/dropout/random_uniform/RandomUniformRandomUniformdropout/dropout/Shape:output:0*
T0*,
_output_shapes
:����������*
dtype0c
dropout/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/dropout/GreaterEqualGreaterEqual5dropout/dropout/random_uniform/RandomUniform:output:0'dropout/dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:�����������
dropout/dropout/CastCast dropout/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:�����������
dropout/dropout/Mul_1Muldropout/dropout/Mul:z:0dropout/dropout/Cast:y:0*
T0*,
_output_shapes
:����������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/dropout/Mul_1:z:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������\
dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout_1/dropout/MulMul&global_average_pooling1d/Mean:output:0 dropout_1/dropout/Const:output:0*
T0*'
_output_shapes
:���������m
dropout_1/dropout/ShapeShape&global_average_pooling1d/Mean:output:0*
T0*
_output_shapes
:�
.dropout_1/dropout/random_uniform/RandomUniformRandomUniform dropout_1/dropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0e
 dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout_1/dropout/GreaterEqualGreaterEqual7dropout_1/dropout/random_uniform/RandomUniform:output:0)dropout_1/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:����������
dropout_1/dropout/CastCast"dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:����������
dropout_1/dropout/Mul_1Muldropout_1/dropout/Mul:z:0dropout_1/dropout/Cast:y:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/dropout/Mul_1:z:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�o
�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233756

inputsO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	?
,sequential_embedding_embedding_lookup_233739:	�NA
/sequential_dense_matmul_readvariableop_resource:>
0sequential_dense_biasadd_readvariableop_resource:
identity��'sequential/dense/BiasAdd/ReadVariableOp�&sequential/dense/MatMul/ReadVariableOp�%sequential/embedding/embedding_lookup�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Z
text_vectorization/StringLowerStringLowerinputs*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*
pattern<br />*
rewrite �
'text_vectorization/StaticRegexReplace_1StaticRegexReplace.text_vectorization/StaticRegexReplace:output:0*#
_output_shapes
:���������*A
pattern64[!"\#\$%\&'\(\)\*\+,\-\./:;<=>\?@\[\\\]\^_`\{\|\}\~]*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV20text_vectorization/StaticRegexReplace_1:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
sequential/CastCast?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*

DstT0*

SrcT0	*(
_output_shapes
:����������x
sequential/embedding/CastCastsequential/Cast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
%sequential/embedding/embedding_lookupResourceGather,sequential_embedding_embedding_lookup_233739sequential/embedding/Cast:y:0*
Tindices0*?
_class5
31loc:@sequential/embedding/embedding_lookup/233739*,
_output_shapes
:����������*
dtype0�
.sequential/embedding/embedding_lookup/IdentityIdentity.sequential/embedding/embedding_lookup:output:0*
T0*?
_class5
31loc:@sequential/embedding/embedding_lookup/233739*,
_output_shapes
:�����������
0sequential/embedding/embedding_lookup/Identity_1Identity7sequential/embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:�����������
sequential/dropout/IdentityIdentity9sequential/embedding/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:����������|
:sequential/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
(sequential/global_average_pooling1d/MeanMean$sequential/dropout/Identity:output:0Csequential/global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:����������
sequential/dropout_1/IdentityIdentity1sequential/global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
&sequential/dense/MatMul/ReadVariableOpReadVariableOp/sequential_dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
sequential/dense/MatMulMatMul&sequential/dropout_1/Identity:output:0.sequential/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
'sequential/dense/BiasAdd/ReadVariableOpReadVariableOp0sequential_dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential/dense/BiasAddBiasAdd!sequential/dense/MatMul:product:0/sequential/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������t
activation_1/SigmoidSigmoid!sequential/dense/BiasAdd:output:0*
T0*'
_output_shapes
:���������g
IdentityIdentityactivation_1/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp(^sequential/dense/BiasAdd/ReadVariableOp'^sequential/dense/MatMul/ReadVariableOp&^sequential/embedding/embedding_lookup?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2R
'sequential/dense/BiasAdd/ReadVariableOp'sequential/dense/BiasAdd/ReadVariableOp2P
&sequential/dense/MatMul/ReadVariableOp&sequential/dense/MatMul/ReadVariableOp2N
%sequential/embedding/embedding_lookup%sequential/embedding/embedding_lookup2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
__inference_save_fn_234157
checkpoint_keyP
Lmutablehashtable_lookup_table_export_values_lookuptableexportv2_table_handle
identity

identity_1

identity_2

identity_3

identity_4

identity_5	��?MutableHashTable_lookup_table_export_values/LookupTableExportV2�
?MutableHashTable_lookup_table_export_values/LookupTableExportV2LookupTableExportV2Lmutablehashtable_lookup_table_export_values_lookuptableexportv2_table_handle",/job:localhost/replica:0/task:0/device:CPU:0*
Tkeys0*
Tvalues0	*
_output_shapes

::P
add/yConst*
_output_shapes
: *
dtype0*
valueB B
table-keysK
addAddcheckpoint_keyadd/y:output:0*
T0*
_output_shapes
: T
add_1/yConst*
_output_shapes
: *
dtype0*
valueB Btable-valuesO
add_1Addcheckpoint_keyadd_1/y:output:0*
T0*
_output_shapes
: E
IdentityIdentityadd:z:0^NoOp*
T0*
_output_shapes
: F
ConstConst*
_output_shapes
: *
dtype0*
valueB B N

Identity_1IdentityConst:output:0^NoOp*
T0*
_output_shapes
: �

Identity_2IdentityFMutableHashTable_lookup_table_export_values/LookupTableExportV2:keys:0^NoOp*
T0*
_output_shapes
:I

Identity_3Identity	add_1:z:0^NoOp*
T0*
_output_shapes
: H
Const_1Const*
_output_shapes
: *
dtype0*
valueB B P

Identity_4IdentityConst_1:output:0^NoOp*
T0*
_output_shapes
: �

Identity_5IdentityHMutableHashTable_lookup_table_export_values/LookupTableExportV2:values:0^NoOp*
T0	*
_output_shapes
:�
NoOpNoOp@^MutableHashTable_lookup_table_export_values/LookupTableExportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : 2�
?MutableHashTable_lookup_table_export_values/LookupTableExportV2?MutableHashTable_lookup_table_export_values/LookupTableExportV2:F B

_output_shapes
: 
(
_user_specified_namecheckpoint_key
�_
�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233417

inputsO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	$
sequential_233408:	�N#
sequential_233410:
sequential_233412:
identity��"sequential/StatefulPartitionedCall�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Z
text_vectorization/StringLowerStringLowerinputs*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*
pattern<br />*
rewrite �
'text_vectorization/StaticRegexReplace_1StaticRegexReplace.text_vectorization/StaticRegexReplace:output:0*#
_output_shapes
:���������*A
pattern64[!"\#\$%\&'\(\)\*\+,\-\./:;<=>\?@\[\\\]\^_`\{\|\}\~]*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV20text_vectorization/StaticRegexReplace_1:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
"sequential/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0sequential_233408sequential_233410sequential_233412*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233327�
activation_1/PartitionedCallPartitionedCall+sequential/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_activation_1_layer_call_and_return_conditional_losses_233253t
IdentityIdentity%activation_1/PartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp#^sequential/StatefulPartitionedCall?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2H
"sequential/StatefulPartitionedCall"sequential/StatefulPartitionedCall2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
/
__inference__initializer_234133
identityG
ConstConst*
_output_shapes
: *
dtype0*
value	B :E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
-
__inference__destroyer_234138
identityG
ConstConst*
_output_shapes
: *
dtype0*
value	B :E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�_
�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233256

inputsO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	$
sequential_233241:	�N#
sequential_233243:
sequential_233245:
identity��"sequential/StatefulPartitionedCall�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Z
text_vectorization/StringLowerStringLowerinputs*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*
pattern<br />*
rewrite �
'text_vectorization/StaticRegexReplace_1StaticRegexReplace.text_vectorization/StaticRegexReplace:output:0*#
_output_shapes
:���������*A
pattern64[!"\#\$%\&'\(\)\*\+,\-\./:;<=>\?@\[\\\]\^_`\{\|\}\~]*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV20text_vectorization/StaticRegexReplace_1:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
"sequential/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0sequential_233241sequential_233243sequential_233245*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233240�
activation_1/PartitionedCallPartitionedCall+sequential/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_activation_1_layer_call_and_return_conditional_losses_233253t
IdentityIdentity%activation_1/PartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp#^sequential/StatefulPartitionedCall?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2H
"sequential/StatefulPartitionedCall"sequential/StatefulPartitionedCall2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�

*__inference_embedding_layer_call_fn_234011

inputs
unknown:	�N
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_232978|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*4
_output_shapes"
 :������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:������������������: 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�7
�	
__inference__traced_save_234281
file_prefix3
/savev2_embedding_embeddings_read_readvariableop+
'savev2_dense_kernel_read_readvariableop)
%savev2_dense_bias_read_readvariableopJ
Fsavev2_mutablehashtable_lookup_table_export_values_lookuptableexportv2L
Hsavev2_mutablehashtable_lookup_table_export_values_lookuptableexportv2_1	(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop&
"savev2_total_3_read_readvariableop&
"savev2_count_3_read_readvariableop&
"savev2_total_2_read_readvariableop&
"savev2_count_2_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop:
6savev2_adam_embedding_embeddings_m_read_readvariableop2
.savev2_adam_dense_kernel_m_read_readvariableop0
,savev2_adam_dense_bias_m_read_readvariableop:
6savev2_adam_embedding_embeddings_v_read_readvariableop2
.savev2_adam_dense_kernel_v_read_readvariableop0
,savev2_adam_dense_bias_v_read_readvariableop
savev2_const_6

identity_1��MergeV2Checkpointsw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: �
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEBFlayer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/table-keysBHlayer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/table-valuesB>layer_with_weights-1/optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-1/optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-1/optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-1/optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEBGlayer_with_weights-1/optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B �	
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0/savev2_embedding_embeddings_read_readvariableop'savev2_dense_kernel_read_readvariableop%savev2_dense_bias_read_readvariableopFsavev2_mutablehashtable_lookup_table_export_values_lookuptableexportv2Hsavev2_mutablehashtable_lookup_table_export_values_lookuptableexportv2_1$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop"savev2_total_3_read_readvariableop"savev2_count_3_read_readvariableop"savev2_total_2_read_readvariableop"savev2_count_2_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop6savev2_adam_embedding_embeddings_m_read_readvariableop.savev2_adam_dense_kernel_m_read_readvariableop,savev2_adam_dense_bias_m_read_readvariableop6savev2_adam_embedding_embeddings_v_read_readvariableop.savev2_adam_dense_kernel_v_read_readvariableop,savev2_adam_dense_bias_v_read_readvariableopsavev2_const_6"/device:CPU:0*
_output_shapes
 *'
dtypes
2		�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 f
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: Q

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: [
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*�
_input_shapesy
w: :	�N::::: : : : : : : : : : : : : :	�N:::	�N::: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:%!

_output_shapes
:	�N:$ 

_output_shapes

:: 

_output_shapes
::

_output_shapes
::

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :%!

_output_shapes
:	�N:$ 

_output_shapes

:: 

_output_shapes
::%!

_output_shapes
:	�N:$ 

_output_shapes

:: 

_output_shapes
::

_output_shapes
: 
�
�
&__inference_dense_layer_call_fn_234095

inputs
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *J
fERC
A__inference_dense_layer_call_and_return_conditional_losses_233007o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
a
C__inference_dropout_layer_call_and_return_conditional_losses_232987

inputs

identity_1[
IdentityIdentityinputs*
T0*4
_output_shapes"
 :������������������h

Identity_1IdentityIdentity:output:0*
T0*4
_output_shapes"
 :������������������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�
-
__inference__destroyer_234123
identityG
ConstConst*
_output_shapes
: *
dtype0*
value	B :E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�	
�
-__inference_sequential_2_layer_call_fn_233273
text_vectorization_input
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�N
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalltext_vectorization_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_sequential_2_layer_call_and_return_conditional_losses_233256o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
__inference_restore_fn_234165
restored_tensors_0
restored_tensors_1	C
?mutablehashtable_table_restore_lookuptableimportv2_table_handle
identity��2MutableHashTable_table_restore/LookupTableImportV2�
2MutableHashTable_table_restore/LookupTableImportV2LookupTableImportV2?mutablehashtable_table_restore_lookuptableimportv2_table_handlerestored_tensors_0restored_tensors_1",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
 G
ConstConst*
_output_shapes
: *
dtype0*
value	B :L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: {
NoOpNoOp3^MutableHashTable_table_restore/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes

::: 2h
2MutableHashTable_table_restore/LookupTableImportV22MutableHashTable_table_restore/LookupTableImportV2:L H

_output_shapes
:
,
_user_specified_namerestored_tensors_0:LH

_output_shapes
:
,
_user_specified_namerestored_tensors_1
�%
�
F__inference_sequential_layer_call_and_return_conditional_losses_233994

inputs	4
!embedding_embedding_lookup_233964:	�N6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookupV
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:����������b
embedding/CastCastCast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
embedding/embedding_lookupResourceGather!embedding_embedding_lookup_233964embedding/Cast:y:0*
Tindices0*4
_class*
(&loc:@embedding/embedding_lookup/233964*,
_output_shapes
:����������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*4
_class*
(&loc:@embedding/embedding_lookup/233964*,
_output_shapes
:�����������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������Z
dropout/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout/dropout/MulMul.embedding/embedding_lookup/Identity_1:output:0dropout/dropout/Const:output:0*
T0*,
_output_shapes
:����������s
dropout/dropout/ShapeShape.embedding/embedding_lookup/Identity_1:output:0*
T0*
_output_shapes
:�
,dropout/dropout/random_uniform/RandomUniformRandomUniformdropout/dropout/Shape:output:0*
T0*,
_output_shapes
:����������*
dtype0c
dropout/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/dropout/GreaterEqualGreaterEqual5dropout/dropout/random_uniform/RandomUniform:output:0'dropout/dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:�����������
dropout/dropout/CastCast dropout/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:�����������
dropout/dropout/Mul_1Muldropout/dropout/Mul:z:0dropout/dropout/Cast:y:0*
T0*,
_output_shapes
:����������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/dropout/Mul_1:z:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������\
dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout_1/dropout/MulMul&global_average_pooling1d/Mean:output:0 dropout_1/dropout/Const:output:0*
T0*'
_output_shapes
:���������m
dropout_1/dropout/ShapeShape&global_average_pooling1d/Mean:output:0*
T0*
_output_shapes
:�
.dropout_1/dropout/random_uniform/RandomUniformRandomUniform dropout_1/dropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0e
 dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout_1/dropout/GreaterEqualGreaterEqual7dropout_1/dropout/random_uniform/RandomUniform:output:0)dropout_1/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:����������
dropout_1/dropout/CastCast"dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:����������
dropout_1/dropout/Mul_1Muldropout_1/dropout/Mul:z:0dropout_1/dropout/Cast:y:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/dropout/Mul_1:z:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�	
�
-__inference_sequential_2_layer_call_fn_233686

inputs
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�N
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_sequential_2_layer_call_and_return_conditional_losses_233417o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
+__inference_sequential_layer_call_fn_233023
embedding_input
unknown:	�N
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallembedding_inputunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_233014o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:a ]
0
_output_shapes
:������������������
)
_user_specified_nameembedding_input
�
c
*__inference_dropout_1_layer_call_fn_234069

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_233053o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
d
H__inference_activation_1_layer_call_and_return_conditional_losses_234004

inputs
identityL
SigmoidSigmoidinputs*
T0*'
_output_shapes
:���������S
IdentityIdentitySigmoid:y:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
F__inference_sequential_layer_call_and_return_conditional_losses_233150
embedding_input#
embedding_233138:	�N
dense_233144:
dense_233146:
identity��dense/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�
!embedding/StatefulPartitionedCallStatefulPartitionedCallembedding_inputembedding_233138*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_232978�
dropout/PartitionedCallPartitionedCall*embedding/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_dropout_layer_call_and_return_conditional_losses_232987�
(global_average_pooling1d/PartitionedCallPartitionedCall dropout/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *]
fXRV
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_232958�
dropout_1/PartitionedCallPartitionedCall1global_average_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_232995�
dense/StatefulPartitionedCallStatefulPartitionedCall"dropout_1/PartitionedCall:output:0dense_233144dense_233146*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *J
fERC
A__inference_dense_layer_call_and_return_conditional_losses_233007u
IdentityIdentity&dense/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/StatefulPartitionedCall"^embedding/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall:a ]
0
_output_shapes
:������������������
)
_user_specified_nameembedding_input
�
c
E__inference_dropout_1_layer_call_and_return_conditional_losses_232995

inputs

identity_1N
IdentityIdentityinputs*
T0*'
_output_shapes
:���������[

Identity_1IdentityIdentity:output:0*
T0*'
_output_shapes
:���������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
F__inference_sequential_layer_call_and_return_conditional_losses_233904

inputs4
!embedding_embedding_lookup_233888:	�N6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookuph
embedding/CastCastinputs*

DstT0*

SrcT0*0
_output_shapes
:�������������������
embedding/embedding_lookupResourceGather!embedding_embedding_lookup_233888embedding/Cast:y:0*
Tindices0*4
_class*
(&loc:@embedding/embedding_lookup/233888*4
_output_shapes"
 :������������������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*4
_class*
(&loc:@embedding/embedding_lookup/233888*4
_output_shapes"
 :�������������������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*4
_output_shapes"
 :�������������������
dropout/IdentityIdentity.embedding/embedding_lookup/Identity_1:output:0*
T0*4
_output_shapes"
 :������������������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/Identity:output:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������x
dropout_1/IdentityIdentity&global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/Identity:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�

b
C__inference_dropout_layer_call_and_return_conditional_losses_233076

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?q
dropout/MulMulinputsdropout/Const:output:0*
T0*4
_output_shapes"
 :������������������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*4
_output_shapes"
 :������������������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :������������������|
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :������������������v
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*4
_output_shapes"
 :������������������f
IdentityIdentitydropout/Mul_1:z:0*
T0*4
_output_shapes"
 :������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�
a
C__inference_dropout_layer_call_and_return_conditional_losses_234036

inputs

identity_1[
IdentityIdentityinputs*
T0*4
_output_shapes"
 :������������������h

Identity_1IdentityIdentity:output:0*
T0*4
_output_shapes"
 :������������������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�
I
-__inference_activation_1_layer_call_fn_233999

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_activation_1_layer_call_and_return_conditional_losses_233253`
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�	
�
A__inference_dense_layer_call_and_return_conditional_losses_234105

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������_
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:���������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
F__inference_sequential_layer_call_and_return_conditional_losses_233165
embedding_input#
embedding_233153:	�N
dense_233159:
dense_233161:
identity��dense/StatefulPartitionedCall�dropout/StatefulPartitionedCall�!dropout_1/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�
!embedding/StatefulPartitionedCallStatefulPartitionedCallembedding_inputembedding_233153*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_embedding_layer_call_and_return_conditional_losses_232978�
dropout/StatefulPartitionedCallStatefulPartitionedCall*embedding/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_dropout_layer_call_and_return_conditional_losses_233076�
(global_average_pooling1d/PartitionedCallPartitionedCall(dropout/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *]
fXRV
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_232958�
!dropout_1/StatefulPartitionedCallStatefulPartitionedCall1global_average_pooling1d/PartitionedCall:output:0 ^dropout/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_dropout_1_layer_call_and_return_conditional_losses_233053�
dense/StatefulPartitionedCallStatefulPartitionedCall*dropout_1/StatefulPartitionedCall:output:0dense_233159dense_233161*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *J
fERC
A__inference_dense_layer_call_and_return_conditional_losses_233007u
IdentityIdentity&dense/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/StatefulPartitionedCall ^dropout/StatefulPartitionedCall"^dropout_1/StatefulPartitionedCall"^embedding/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dropout/StatefulPartitionedCalldropout/StatefulPartitionedCall2F
!dropout_1/StatefulPartitionedCall!dropout_1/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall:a ]
0
_output_shapes
:������������������
)
_user_specified_nameembedding_input"�L
saver_filename:0StatefulPartitionedCall_2:0StatefulPartitionedCall_38"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
Y
text_vectorization_input=
*serving_default_text_vectorization_input:0���������B
activation_12
StatefulPartitionedCall_1:0���������tensorflow/serving/predict:��
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer-2
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*	&call_and_return_all_conditional_losses

_default_save_signature
	optimizer

signatures"
_tf_keras_sequential
P
	keras_api
_lookup_layer
_adapt_function"
_tf_keras_layer
�
layer_with_weights-0
layer-0
layer-1
layer-2
layer-3
layer_with_weights-1
layer-4
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
	optimizer"
_tf_keras_sequential
�
	variables
trainable_variables
regularization_losses
	keras_api
 __call__
*!&call_and_return_all_conditional_losses"
_tf_keras_layer
5
"1
#2
$3"
trackable_list_wrapper
5
"0
#1
$2"
trackable_list_wrapper
 "
trackable_list_wrapper
�
%non_trainable_variables

&layers
'metrics
(layer_regularization_losses
)layer_metrics
	variables
trainable_variables
regularization_losses
__call__

_default_save_signature
*	&call_and_return_all_conditional_losses
&	"call_and_return_conditional_losses"
_generic_user_object
�
*trace_0
+trace_1
,trace_2
-trace_32�
-__inference_sequential_2_layer_call_fn_233273
-__inference_sequential_2_layer_call_fn_233667
-__inference_sequential_2_layer_call_fn_233686
-__inference_sequential_2_layer_call_fn_233453�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 z*trace_0z+trace_1z,trace_2z-trace_3
�
.trace_0
/trace_1
0trace_2
1trace_32�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233756
H__inference_sequential_2_layer_call_and_return_conditional_losses_233840
H__inference_sequential_2_layer_call_and_return_conditional_losses_233513
H__inference_sequential_2_layer_call_and_return_conditional_losses_233573�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 z.trace_0z/trace_1z0trace_2z1trace_3
�B�
!__inference__wrapped_model_232948text_vectorization_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
"
	optimizer
,
2serving_default"
signature_map
"
_generic_user_object
L
3	keras_api
4lookup_table
5token_counts"
_tf_keras_layer
�
6trace_02�
__inference_adapt_step_233642�
���
FullArgSpec
args�

jiterator
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z6trace_0
�
7	variables
8trainable_variables
9regularization_losses
:	keras_api
;__call__
*<&call_and_return_all_conditional_losses
"
embeddings"
_tf_keras_layer
�
=	variables
>trainable_variables
?regularization_losses
@	keras_api
A__call__
*B&call_and_return_all_conditional_losses
C_random_generator"
_tf_keras_layer
�
D	variables
Etrainable_variables
Fregularization_losses
G	keras_api
H__call__
*I&call_and_return_all_conditional_losses"
_tf_keras_layer
�
J	variables
Ktrainable_variables
Lregularization_losses
M	keras_api
N__call__
*O&call_and_return_all_conditional_losses
P_random_generator"
_tf_keras_layer
�
Q	variables
Rtrainable_variables
Sregularization_losses
T	keras_api
U__call__
*V&call_and_return_all_conditional_losses

#kernel
$bias"
_tf_keras_layer
5
"0
#1
$2"
trackable_list_wrapper
5
"0
#1
$2"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Wnon_trainable_variables

Xlayers
Ymetrics
Zlayer_regularization_losses
[layer_metrics
	variables
trainable_variables
regularization_losses
__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
�
\trace_0
]trace_1
^trace_2
_trace_3
`trace_4
atrace_52�
+__inference_sequential_layer_call_fn_233023
+__inference_sequential_layer_call_fn_233851
+__inference_sequential_layer_call_fn_233862
+__inference_sequential_layer_call_fn_233135
+__inference_sequential_layer_call_fn_233873
+__inference_sequential_layer_call_fn_233884�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 z\trace_0z]trace_1z^trace_2z_trace_3z`trace_4zatrace_5
�
btrace_0
ctrace_1
dtrace_2
etrace_3
ftrace_4
gtrace_52�
F__inference_sequential_layer_call_and_return_conditional_losses_233904
F__inference_sequential_layer_call_and_return_conditional_losses_233938
F__inference_sequential_layer_call_and_return_conditional_losses_233150
F__inference_sequential_layer_call_and_return_conditional_losses_233165
F__inference_sequential_layer_call_and_return_conditional_losses_233959
F__inference_sequential_layer_call_and_return_conditional_losses_233994�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 zbtrace_0zctrace_1zdtrace_2zetrace_3zftrace_4zgtrace_5
�
hiter

ibeta_1

jbeta_2
	kdecay
llearning_rate"m�#m�$m�"v�#v�$v�"
	optimizer
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
mnon_trainable_variables

nlayers
ometrics
player_regularization_losses
qlayer_metrics
	variables
trainable_variables
regularization_losses
 __call__
*!&call_and_return_all_conditional_losses
&!"call_and_return_conditional_losses"
_generic_user_object
�
rtrace_02�
-__inference_activation_1_layer_call_fn_233999�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zrtrace_0
�
strace_02�
H__inference_activation_1_layer_call_and_return_conditional_losses_234004�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zstrace_0
':%	�N2embedding/embeddings
:2dense/kernel
:2
dense/bias
 "
trackable_list_wrapper
5
0
1
2"
trackable_list_wrapper
.
t0
u1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
-__inference_sequential_2_layer_call_fn_233273text_vectorization_input"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
-__inference_sequential_2_layer_call_fn_233667inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
-__inference_sequential_2_layer_call_fn_233686inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
-__inference_sequential_2_layer_call_fn_233453text_vectorization_input"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233756inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233840inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233513text_vectorization_input"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233573text_vectorization_input"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
$__inference_signature_wrapper_233594text_vectorization_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
"
_generic_user_object
f
v_initializer
w_create_resource
x_initialize
y_destroy_resourceR jtf.StaticHashTable
L
z_create_resource
{_initialize
|_destroy_resourceR Z

 ��
�B�
__inference_adapt_step_233642iterator"�
���
FullArgSpec
args�

jiterator
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
'
"0"
trackable_list_wrapper
'
"0"
trackable_list_wrapper
 "
trackable_list_wrapper
�
}non_trainable_variables

~layers
metrics
 �layer_regularization_losses
�layer_metrics
7	variables
8trainable_variables
9regularization_losses
;__call__
*<&call_and_return_all_conditional_losses
&<"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
*__inference_embedding_layer_call_fn_234011�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
E__inference_embedding_layer_call_and_return_conditional_losses_234021�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
=	variables
>trainable_variables
?regularization_losses
A__call__
*B&call_and_return_all_conditional_losses
&B"call_and_return_conditional_losses"
_generic_user_object
�
�trace_0
�trace_12�
(__inference_dropout_layer_call_fn_234026
(__inference_dropout_layer_call_fn_234031�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 z�trace_0z�trace_1
�
�trace_0
�trace_12�
C__inference_dropout_layer_call_and_return_conditional_losses_234036
C__inference_dropout_layer_call_and_return_conditional_losses_234048�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 z�trace_0z�trace_1
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
D	variables
Etrainable_variables
Fregularization_losses
H__call__
*I&call_and_return_all_conditional_losses
&I"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
9__inference_global_average_pooling1d_layer_call_fn_234053�
���
FullArgSpec%
args�
jself
jinputs
jmask
varargs
 
varkw
 
defaults�

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_234059�
���
FullArgSpec%
args�
jself
jinputs
jmask
varargs
 
varkw
 
defaults�

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
J	variables
Ktrainable_variables
Lregularization_losses
N__call__
*O&call_and_return_all_conditional_losses
&O"call_and_return_conditional_losses"
_generic_user_object
�
�trace_0
�trace_12�
*__inference_dropout_1_layer_call_fn_234064
*__inference_dropout_1_layer_call_fn_234069�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 z�trace_0z�trace_1
�
�trace_0
�trace_12�
E__inference_dropout_1_layer_call_and_return_conditional_losses_234074
E__inference_dropout_1_layer_call_and_return_conditional_losses_234086�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 z�trace_0z�trace_1
"
_generic_user_object
.
#0
$1"
trackable_list_wrapper
.
#0
$1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
Q	variables
Rtrainable_variables
Sregularization_losses
U__call__
*V&call_and_return_all_conditional_losses
&V"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
&__inference_dense_layer_call_fn_234095�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
A__inference_dense_layer_call_and_return_conditional_losses_234105�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
 "
trackable_list_wrapper
C
0
1
2
3
4"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
+__inference_sequential_layer_call_fn_233023embedding_input"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
+__inference_sequential_layer_call_fn_233851inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
+__inference_sequential_layer_call_fn_233862inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
+__inference_sequential_layer_call_fn_233135embedding_input"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
+__inference_sequential_layer_call_fn_233873inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
+__inference_sequential_layer_call_fn_233884inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
F__inference_sequential_layer_call_and_return_conditional_losses_233904inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
F__inference_sequential_layer_call_and_return_conditional_losses_233938inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
F__inference_sequential_layer_call_and_return_conditional_losses_233150embedding_input"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
F__inference_sequential_layer_call_and_return_conditional_losses_233165embedding_input"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
F__inference_sequential_layer_call_and_return_conditional_losses_233959inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
F__inference_sequential_layer_call_and_return_conditional_losses_233994inputs"�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
-__inference_activation_1_layer_call_fn_233999inputs"�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
H__inference_activation_1_layer_call_and_return_conditional_losses_234004inputs"�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
R
�	variables
�	keras_api

�total

�count"
_tf_keras_metric
c
�	variables
�	keras_api

�total

�count
�
_fn_kwargs"
_tf_keras_metric
"
_generic_user_object
�
�trace_02�
__inference__creator_234110�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference__initializer_234118�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference__destroyer_234123�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference__creator_234128�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference__initializer_234133�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference__destroyer_234138�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
*__inference_embedding_layer_call_fn_234011inputs"�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
E__inference_embedding_layer_call_and_return_conditional_losses_234021inputs"�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
(__inference_dropout_layer_call_fn_234026inputs"�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
(__inference_dropout_layer_call_fn_234031inputs"�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
C__inference_dropout_layer_call_and_return_conditional_losses_234036inputs"�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
C__inference_dropout_layer_call_and_return_conditional_losses_234048inputs"�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
9__inference_global_average_pooling1d_layer_call_fn_234053inputs"�
���
FullArgSpec%
args�
jself
jinputs
jmask
varargs
 
varkw
 
defaults�

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_234059inputs"�
���
FullArgSpec%
args�
jself
jinputs
jmask
varargs
 
varkw
 
defaults�

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
*__inference_dropout_1_layer_call_fn_234064inputs"�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
*__inference_dropout_1_layer_call_fn_234069inputs"�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
E__inference_dropout_1_layer_call_and_return_conditional_losses_234074inputs"�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
E__inference_dropout_1_layer_call_and_return_conditional_losses_234086inputs"�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
&__inference_dense_layer_call_fn_234095inputs"�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
A__inference_dense_layer_call_and_return_conditional_losses_234105inputs"�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
R
�	variables
�	keras_api

�total

�count"
_tf_keras_metric
c
�	variables
�	keras_api

�total

�count
�
_fn_kwargs"
_tf_keras_metric
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
�B�
__inference__creator_234110"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference__initializer_234118"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference__destroyer_234123"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference__creator_234128"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference__initializer_234133"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference__destroyer_234138"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
,:*	�N2Adam/embedding/embeddings/m
#:!2Adam/dense/kernel/m
:2Adam/dense/bias/m
,:*	�N2Adam/embedding/embeddings/v
#:!2Adam/dense/kernel/v
:2Adam/dense/bias/v
�B�
__inference_save_fn_234157checkpoint_key"�
���
FullArgSpec
args�
jcheckpoint_key
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�	
� 
�B�
__inference_restore_fn_234165restored_tensors_0restored_tensors_1"�
���
FullArgSpec
args� 
varargsjrestored_tensors
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
	�	
J
Constjtf.TrackableConstant
!J	
Const_1jtf.TrackableConstant
!J	
Const_2jtf.TrackableConstant
!J	
Const_3jtf.TrackableConstant
!J	
Const_4jtf.TrackableConstant
!J	
Const_5jtf.TrackableConstant7
__inference__creator_234110�

� 
� "� 7
__inference__creator_234128�

� 
� "� 9
__inference__destroyer_234123�

� 
� "� 9
__inference__destroyer_234138�

� 
� "� B
__inference__initializer_2341184���

� 
� "� ;
__inference__initializer_234133�

� 
� "� �
!__inference__wrapped_model_232948�
4���"#$=�:
3�0
.�+
text_vectorization_input���������
� ";�8
6
activation_1&�#
activation_1����������
H__inference_activation_1_layer_call_and_return_conditional_losses_234004X/�,
%�"
 �
inputs���������
� "%�"
�
0���������
� |
-__inference_activation_1_layer_call_fn_233999K/�,
%�"
 �
inputs���������
� "����������k
__inference_adapt_step_233642J5�?�<
5�2
0�-�
����������IteratorSpec 
� "
 �
A__inference_dense_layer_call_and_return_conditional_losses_234105\#$/�,
%�"
 �
inputs���������
� "%�"
�
0���������
� y
&__inference_dense_layer_call_fn_234095O#$/�,
%�"
 �
inputs���������
� "�����������
E__inference_dropout_1_layer_call_and_return_conditional_losses_234074\3�0
)�&
 �
inputs���������
p 
� "%�"
�
0���������
� �
E__inference_dropout_1_layer_call_and_return_conditional_losses_234086\3�0
)�&
 �
inputs���������
p
� "%�"
�
0���������
� }
*__inference_dropout_1_layer_call_fn_234064O3�0
)�&
 �
inputs���������
p 
� "����������}
*__inference_dropout_1_layer_call_fn_234069O3�0
)�&
 �
inputs���������
p
� "�����������
C__inference_dropout_layer_call_and_return_conditional_losses_234036v@�=
6�3
-�*
inputs������������������
p 
� "2�/
(�%
0������������������
� �
C__inference_dropout_layer_call_and_return_conditional_losses_234048v@�=
6�3
-�*
inputs������������������
p
� "2�/
(�%
0������������������
� �
(__inference_dropout_layer_call_fn_234026i@�=
6�3
-�*
inputs������������������
p 
� "%�"�������������������
(__inference_dropout_layer_call_fn_234031i@�=
6�3
-�*
inputs������������������
p
� "%�"�������������������
E__inference_embedding_layer_call_and_return_conditional_losses_234021q"8�5
.�+
)�&
inputs������������������
� "2�/
(�%
0������������������
� �
*__inference_embedding_layer_call_fn_234011d"8�5
.�+
)�&
inputs������������������
� "%�"�������������������
T__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_234059{I�F
?�<
6�3
inputs'���������������������������

 
� ".�+
$�!
0������������������
� �
9__inference_global_average_pooling1d_layer_call_fn_234053nI�F
?�<
6�3
inputs'���������������������������

 
� "!�������������������z
__inference_restore_fn_234165Y5K�H
A�>
�
restored_tensors_0
�
restored_tensors_1	
� "� �
__inference_save_fn_234157�5&�#
�
�
checkpoint_key 
� "���
`�]

name�
0/name 
#

slice_spec�
0/slice_spec 

tensor�
0/tensor
`�]

name�
1/name 
#

slice_spec�
1/slice_spec 

tensor�
1/tensor	�
H__inference_sequential_2_layer_call_and_return_conditional_losses_233513z
4���"#$E�B
;�8
.�+
text_vectorization_input���������
p 

 
� "%�"
�
0���������
� �
H__inference_sequential_2_layer_call_and_return_conditional_losses_233573z
4���"#$E�B
;�8
.�+
text_vectorization_input���������
p

 
� "%�"
�
0���������
� �
H__inference_sequential_2_layer_call_and_return_conditional_losses_233756h
4���"#$3�0
)�&
�
inputs���������
p 

 
� "%�"
�
0���������
� �
H__inference_sequential_2_layer_call_and_return_conditional_losses_233840h
4���"#$3�0
)�&
�
inputs���������
p

 
� "%�"
�
0���������
� �
-__inference_sequential_2_layer_call_fn_233273m
4���"#$E�B
;�8
.�+
text_vectorization_input���������
p 

 
� "�����������
-__inference_sequential_2_layer_call_fn_233453m
4���"#$E�B
;�8
.�+
text_vectorization_input���������
p

 
� "�����������
-__inference_sequential_2_layer_call_fn_233667[
4���"#$3�0
)�&
�
inputs���������
p 

 
� "�����������
-__inference_sequential_2_layer_call_fn_233686[
4���"#$3�0
)�&
�
inputs���������
p

 
� "�����������
F__inference_sequential_layer_call_and_return_conditional_losses_233150w"#$I�F
?�<
2�/
embedding_input������������������
p 

 
� "%�"
�
0���������
� �
F__inference_sequential_layer_call_and_return_conditional_losses_233165w"#$I�F
?�<
2�/
embedding_input������������������
p

 
� "%�"
�
0���������
� �
F__inference_sequential_layer_call_and_return_conditional_losses_233904n"#$@�=
6�3
)�&
inputs������������������
p 

 
� "%�"
�
0���������
� �
F__inference_sequential_layer_call_and_return_conditional_losses_233938n"#$@�=
6�3
)�&
inputs������������������
p

 
� "%�"
�
0���������
� �
F__inference_sequential_layer_call_and_return_conditional_losses_233959f"#$8�5
.�+
!�
inputs����������	
p 

 
� "%�"
�
0���������
� �
F__inference_sequential_layer_call_and_return_conditional_losses_233994f"#$8�5
.�+
!�
inputs����������	
p

 
� "%�"
�
0���������
� �
+__inference_sequential_layer_call_fn_233023j"#$I�F
?�<
2�/
embedding_input������������������
p 

 
� "�����������
+__inference_sequential_layer_call_fn_233135j"#$I�F
?�<
2�/
embedding_input������������������
p

 
� "�����������
+__inference_sequential_layer_call_fn_233851a"#$@�=
6�3
)�&
inputs������������������
p 

 
� "�����������
+__inference_sequential_layer_call_fn_233862a"#$@�=
6�3
)�&
inputs������������������
p

 
� "�����������
+__inference_sequential_layer_call_fn_233873Y"#$8�5
.�+
!�
inputs����������	
p 

 
� "�����������
+__inference_sequential_layer_call_fn_233884Y"#$8�5
.�+
!�
inputs����������	
p

 
� "�����������
$__inference_signature_wrapper_233594�
4���"#$Y�V
� 
O�L
J
text_vectorization_input.�+
text_vectorization_input���������";�8
6
activation_1&�#
activation_1���������